#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 23.07.13, 17:04
Useful description of the module ''
"""
__author__ = 'Taras.Taturevich'

import socket
import exceptions
import myoracle
import cx_Oracle
import subprocess

pos_info_query = u"""
SELECT t.workstation_id, t.Status_Code, t.employee_id, t.status_timestamp
from GKRETAIL.GK_TENDER_REPOSITORY_STATUS t
WHERE t.Tender_Repository_Id <> 0
and t.workstation_id is not NULL
ORDER BY t.workstation_id
"""

bo_addr_query = "select t.city, t.street from gkretail.gk_store_data t"

start_pos_nums = {
    '6': 20,
    '70': 84,
    '134': 148,
    '198': 212
}

pos_statuses = {
    'N': u'Неиспользуемый/Рассчитаный',
    'O': u'<-Вышел из системы',
    'I': u'->Регистрирован',
    'P': u'Перерыв'
}


class ShopError(exceptions.Exception):
    def __init__(self, value):
        """
        :type value: basestring
        """
        self.value = value

    def __str__(self):
        return repr(self.value)


def get_bonames(listfile):
    """
    Reads text file with list of bo numbers and returns int
    Empty lines of file are ignored, surrounding spaces are stripped
    If non-number there, add line to errors list
    List is sorted by number

    Returns list of bo numbers and list of errors

    :param basestring listfile: Path to file with bo numbers
    :rtype: (list, list)
    """
    bonames_list = []
    errors = []
    with open(listfile, 'r') as f:
        for bo in f.readlines():
            bo = bo.strip('\n').strip()
            if bo == '':
                continue
            bonames_list.append(bo)
            #try:
            #    bonames_list.append(int(bo))
            #except ValueError:
            #    errors.append(u'Cannot parse {} to BO number'.format(bo))

        bonames_list.sort()
    return bonames_list, errors


def get_hostlist(listfile):
    """
    Reads text file with ip list and returns it
    Empty lines of file are ignored, surrounding spaces are stripped
    If non-ip there, add line to errors list
    List is sorted by address

    :param str or unicode listfile: Path to file with bo numbers
    :rtype: (list, list)
    """
    hosts_list = []
    with open(listfile, 'r') as f:
        for line in f.readlines():
            line = line.strip('\n').strip()
            if line == '':
                continue
            hosts_list.append(line)
        hosts_list.sort()
    return hosts_list


def get_iplist(listfile):
    """
    Reads text file with ip list and returns it
    Empty lines of file are ignored, surrounding spaces are stripped
    If non-ip there, add line to errors list
    List is sorted by address

    :param str or unicode listfile: Path to file with bo numbers
    :rtype: (list, list)
    """
    bonames_list = []
    errors = []
    with open(listfile, 'r') as f:
        for ip in f.readlines():
            ip = ip.strip('\n').strip()
            if ip == '':
                continue
            try:
                int(ip.replace('.', ''))
                bonames_list.append(ip)
            except ValueError:
                errors.append(u'Cannot parse {} to ip'.format(ip))

        bonames_list.sort()
    return bonames_list, errors


def get_boip(hostname):
    """
    returns ip address of given hostname

    :param basestring hostname: hostname
    :rtype: str
    """
    try:
        return str(socket.gethostbyname(hostname))
    except socket.gaierror:
        raise ShopError(u'Не могу получить IP адрес магазина {}'.format(hostname))


def get_pos_info(db_conn, bonum, boip, bo_nonstandart=None):
    """

    :param myoracle.OracleConnection db_conn: Oracle db connection object
    :param string or dict boip: basestring
    :param dict bo_nonstandart: dict with
    """
    if bo_nonstandart is None:
        bo_nonstandart = {}
    pos_querydata = db_conn.query(pos_info_query)
    pos_querydata2 = []
    pos_nums_stamps = {}
    for num, (pos_num, pos_status, employee_id, status_timestamp) in enumerate(pos_querydata):
        if pos_num not in pos_nums_stamps or pos_nums_stamps[pos_num][1] < status_timestamp:
            pos_nums_stamps[pos_num] = [num, status_timestamp]

    pos_nums_list = list(pos_nums_stamps.keys())

    for pos_num in pos_nums_list:
        pos_querydata2.append(list(pos_querydata[pos_nums_stamps[pos_num][0]]))

    pos_querydata = pos_querydata2
    last_ip_dot_pos = boip.rfind('.')
    boip_begin = boip[:last_ip_dot_pos + 1]
    last_ip_seg = boip[last_ip_dot_pos + 1:]

    try:
        start_pos_ipnum = start_pos_nums[last_ip_seg]
    except KeyError:
        try:
            start_pos_ipnum = int(bo_nonstandart[bonum])
        except KeyError:
            # print bonum
            if 4000 <= int(bonum) <= 4100:
                start_pos_ipnum = 161
            else:
                raise ShopError, u'Non-standart POS numeration in shop {}'.format(bonum)

    for i, item in enumerate(pos_querydata):
        pos_querydata[i].append(boip_begin + str(item[0] + start_pos_ipnum - 1))
        pos_querydata[i][1] = pos_statuses[pos_querydata[i][1]]

    return pos_querydata, start_pos_ipnum


def ping(ip):
    ret = subprocess.call("ping %s" % ip,
                          shell=False,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE)
    return True if ret == 0 else False


def main():
    """


    """
    bo_num = '8884'
    print 'Get IP-addresses for shop {} and its POS'.format('BO-{}'.format(bo_num))
    bo_ip = get_boip('BO-{}'.format(bo_num))

    try:
        db_conn = myoracle.connect_db_timeout(bo_ip, 1521, 'x5', 'gkretail', 'gkretail', 120)
    except cx_Oracle.DatabaseError:
        exit()

    try:
        # noinspection PyUnboundLocalVariable
        pos_info = get_pos_info(db_conn, bo_num, bo_ip)
    except ShopError as r:
        print r.value
        exit()

    # noinspection PyUnboundLocalVariable
    print '\n'.join((', '.join(unicode(x) for x in y) for y in pos_info))
    db_conn.close()


if __name__ == "__main__":
    main()