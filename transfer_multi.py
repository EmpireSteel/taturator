#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 25.07.13, 18:25
Useful description of the module ''
"""
__author__ = 'Taras.Taturevich'

import datetime
import time
# from threading import Thread
# from Queue import Queue
import db
#import gc
# import traceback

from os import path
from send_to_bo import send_2bo
from send_to_pos import post_production
from save_stat import SaveStat, save_log
from multiqueue import ManageQueue


#gc.disable()
#gc.set_debug(gc.DEBUG_LEAK)


# def write_gc():
#     #print gc.collect()
#     pass
#     #with open('garbage.txt', 'a') as f:
#     #    f.write(str(gc.collect()))


def check_right_time(start_time_str, stop_time_str):
    curr_time = datetime.datetime.now()
    start_hour, start_minute = start_time_str.split(':')
    stop_hour, stop_minute = stop_time_str.split(':')

    start_time = datetime.datetime(curr_time.year, curr_time.month,
                                   curr_time. day, int(start_hour), int(start_minute))

    stop_time = datetime.datetime(curr_time.year, curr_time.month,
                                  curr_time.day, int(stop_hour), int(stop_minute))

    if stop_time > start_time:
        return True if start_time < curr_time < stop_time else False
    else:
        return True if curr_time < stop_time or curr_time > start_time else False


class ExchangeObject(object):
    ERROR = u'Error'
    INFO = u'Info'

    def __init__(self, connection):

        #send_id, sent, total, speed, block_time, percent_done, block_size, type:bo|pos|pos_fin|bo_nc|bo_fin|bo_upd
        self.snd_statistics = []

        #send_num : try_num, send_stamp, locked, uploaded, archived
        # host_id, file_id, restart, 'hostname', 'connect'
        self.bo_snd_status = {}

        #send_num: trynum, hostname, pos_info, locked, completed, stamp, restart
        #posinfo - posnum: [pos_status, cashier, pos_stamp, ip, stage]
        self.pos_snd_status = {}

        self.message_table = []
        tb_params = db.Params(connection)

        self.enabled_time = None
        self.login = tb_params['bologin']
        self.password = tb_params['bopass']
        self.blocksize = tb_params['blocksize']
        self.conntimeout = int(tb_params['conn_timeout'])
        self.thread_num = int(tb_params['bosendthreads'])

        self.pos_login = tb_params['pos_login']
        self.pos_pwd = tb_params['pos_pwd']

        self.ora_port = tb_params['ora_port']
        self.ora_login = tb_params['ora_login']
        self.ora_pwd = tb_params['ora_pwd']
        self.ora_sid = tb_params['ora_sid']
        self.ora_conn_timeout = int(tb_params['ora_conn_timeout'])
        self.pos_exclusions = []
        self.bo_nonstandart = {}
        self.addrs_style = u''

        self.post_proc = {}

        self.localpath = ''
        self.filesize = 0
        self.remotepath = ''
        self.pospath = ''
        self.deflatepath = ''
        self.scriptpath = ''
        self.posspeed = 0
        self.postime = 0
        self.grey_pos_days = 0

        self.stages = {}

    def update_db(self, send_id):
        """
        :type send_id: int
        """
        self.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})

    def update_posdb(self, send_id):
        """
        :type send_id: int
        """
        self.snd_statistics.append({'send_id': send_id, 'type': 'pos'})

    def add_message(self, send_id, msg_type, message):
        self.message_table.append((send_id, msg_type, message, time.time()))

    def check_enabled_state(self):
        return self.enabled_time

    def set_local_path(self, filepath):
        self.localpath = filepath
        self.filesize = path.getsize(filepath)

    def set_posspeed(self, posspeed):
        """
        :type posspeed: float or int
        """
        self.posspeed = posspeed
        self.postime = self.filesize / (self.posspeed * 1024.0)

    def add_update_req(self, send_id, type_):
        """
        :type send_id: int
        :type type_: str or unicode
        """
        self.snd_statistics.append({'send_id': send_id, 'type': type_})

    def del_possend(self, send_id):
        """
        :type send_id: int
        """
        self.snd_statistics.append({'send_id': send_id, 'type': 'remove_possend'})


def task_processor(data):
    """
    :type data: (int, ExchangeObject)
    """
    send_id, eo = data

    while not eo.enabled_time:
        time.sleep(10)

    stop_stage = eo.post_proc['stop']
    snd_status = eo.bo_snd_status[send_id]
    snd_status['locked'] = True
    snd_status['curr_error'] = ''
    ret = False
    try:
        if snd_status['stage'] == 0:
            send_2bo(send_id, eo)

        if snd_status['stage'] > 0:
            post_production(send_id, eo)

        if snd_status['stage'] == stop_stage:
            ret = True
        else:
            ret = False
    except:
        return False
    finally:
        snd_status['locked'] = False
        snd_status['send_stamp'] = time.time()
        eo.update_db(send_id)
        return ret


class Runner(object):
    def __init__(self, connection):
        self.connection = connection
        self.tb_bosend = db.BOSend(connection)
        self.tb_files = db.Files(connection)
        self.tb_hosts = db.Hosts(connection)
        self.tb_sendstat = db.SendStat(connection)
        self.tb_messages = db.SendMessages(connection)
        self.tb_params = db.Params(connection)
        self.tb_possend = db.POSSend(connection)

        self.start_time = self.tb_params['start_time']
        self.stop_time = self.tb_params['stop_time']
        self.send_pause = int(self.tb_params['send_pause'])
        self.thread_num = int(self.tb_params['bosendthreads'])
        self.post_thread_num = int(self.tb_params['post_threads'])
        self.refresh_time = int(self.tb_params['refresh_time'])
        self.stat_dir = path.abspath(self.tb_params['stat_dir'])
        self.stat_template = path.abspath(self.tb_params['stat_template'])
        self.grey_pos_days = int(self.tb_params['grey_pos_days'])
        self.log_dir = self.tb_params['log_dir']
        self.log_max_lines = int(self.tb_params['max_log_lines'])

        with open(self.tb_params['pos_exclusions']) as f:
            self.pos_exclusions = [x.strip(' ') for x in f.read().split('\n')]

        with open(self.tb_params['bo_nonstandart']) as f:
            try:
                # noinspection PyTypeChecker
                self.bo_nonstandart = dict([x.strip(' ').split(' ') for x in f.read().split('\n')])
            except ValueError:
                pass

        self.enabled_time_current = True

        self.tb_bosend.edit('locked', 1, 'locked', 0)
        self.tb_bosend.edit('restart', 1, ('stage', 'tries', 'sent_time'), (0, 0, 0))

        self.qmanager = ManageQueue(self.thread_num, task_processor,
                                    rest_timeout=self.refresh_time, verbose=True)
        self.qmanager.start_queues()
        # self.qmanager_PP = ManageQueue(self.thread_num, post_production_multi,
        #                                rest_timeout=self.refresh_time, verbose=True)
        # self.qmanager_PP.start_queues()

        self.files = {}

    def fill_files(self):
        files = self.tb_files.get_notstopped(('id', 'filepath', 'remotepath', 'movetopos', 'deflate', 'runscript',
                                              'pospath', 'scriptpath', 'deflatepath', 'addrs_style'))

        for file_id, filepath, remotepath, movetopos, deflate, runscript, pospath, scriptpath, \
                deflatepath, addrs_style in files:

            exch_obj = ExchangeObject(self.connection)
            exch_obj.set_local_path(filepath)
            exch_obj.set_posspeed(int(self.tb_params['pos_send_speed']))
            exch_obj.remotepath = remotepath
            exch_obj.pospath = pospath
            exch_obj.deflatepath = deflatepath
            exch_obj.scriptpath = scriptpath
            exch_obj.pos_exclusions = self.pos_exclusions
            exch_obj.bo_nonstandart = self.bo_nonstandart
            exch_obj.grey_pos_days = self.grey_pos_days
            exch_obj.addrs_style = addrs_style

            exch_obj.enabled_time = self.enabled_time_current

            post_processing = {}
            stop_stage = max((movetopos, deflate, runscript)) + 1
            # exch_obj.post_proc = True if stop_stage > 1 else False

            post_processing['stop'] = stop_stage
            post_processing['movetopos'] = movetopos
            post_processing['deflate'] = deflate
            post_processing['runscript'] = runscript
            exch_obj.post_proc = post_processing

            stat_obj = SaveStat(self.connection, self.stat_dir, self.stat_template, file_id, self.pos_exclusions)
            self.files[file_id] = (exch_obj, stat_obj)

    def fill_send_params(self):

        curr_time = time.time()
        for file_id, (exch_obj, stat_obj) in self.files.iteritems():

            login, password = self.tb_files.get_by_id(file_id, ['login', 'password'])[0]
            login = str(login)
            password = str(password)

            for send_id, stage, host_id, restart,  trynum, tries_pos, \
                in self.tb_bosend.get_by_file_id(file_id, ('id', 'stage', 'host_id', 'restart',
                                                           'tries', 'tries_pos')):

                hostname = self.tb_hosts.get_by_id(host_id, 'hostname')[0][0]
                #
                # if restart == 1:
                #     stage = 0
                #     tries_pos = 0
                if login == 'None' or len(login) == 0:
                    login = exch_obj.login
                if password == 'None' or len(password) == 0:
                    password = exch_obj.password

                exch_obj.bo_snd_status[send_id] = {
                    'trynum': trynum,
                    'send_stamp': curr_time,
                    'locked': False,
                    'host_id': host_id,
                    'file_id': file_id,
                    'restart': restart,
                    'hostname': hostname,
                    'connect': False,
                    'stage': stage,
                    'tries_pos': tries_pos,
                    'curr_error': '',
                    'checked': False,
                    'login': login,
                    'password': password
                }
                self.fill_pos_send_params_singlesendid(exch_obj, send_id)

    def fill_pos_send_params_singlesendid(self, exch_obj, send_id, restart=False):
        curr_time = time.time()
        host_id, restart0, hostname, trynum = self.get_bosend_params_by_id(send_id)
        pos_info = {}
        pos_items = self.tb_possend.get_by_bosend_id(send_id,
                                                     ('stage', 'tries', 'posnum', 'posip', 'pos_status',
                                                      'accessible', 'timestamp'))

        for stage, tries, posnum, posip, pos_status, ping, timestamp in pos_items:
            if posip not in self.pos_exclusions:
                pos_info[posnum] = {'stage': stage,
                                    'tries': tries,
                                    'ip': posip,
                                    'status': pos_status,
                                    'ping': True if ping == 1 else False,
                                    'curr_error': '',
                                    'stamp': timestamp
                                    }

        snd_info = {'trynum': 0,
                    'hostname': hostname,
                    'locked': False,
                    'pos_info': pos_info,
                    'stamp': curr_time,
                    'restart': restart}

        exch_obj.pos_snd_status[send_id] = snd_info

    def check_time_permission(self):
        prev_time_permission = self.enabled_time_current
        self.enabled_time_current = check_right_time(self.start_time, self.stop_time)
        if self.enabled_time_current != prev_time_permission:
            if not self.enabled_time_current:
                print u'Наступило время ограничения передачи, ' \
                      u'передача будет возобновлена в {}'.format(self.start_time)
            else:
                print u'Возобновлена передача данных, передача ' \
                      u'будет приостановлена в {}'.format(self.stop_time)

            for exch_obj, stat_obj in self.files.itervalues():
                exch_obj.enabled_time = self.enabled_time_current

    def get_bosend_by_file_id(self, file_id, stage):
        return [x[0] for x in self.tb_bosend._get_record_by_column_values(('file_id', 'stage'),
                                                                          (file_id, stage), 'id')]

    def get_bosend_params_by_id(self, send_id):
        host_id, restart, trynum = self.tb_bosend.get_by_id(send_id, ('host_id', 'restart', 'tries'))[0]
        hostname = self.tb_hosts.get_by_host(host_id, 'hostname')[0][0]
        return host_id, restart, hostname, trynum

    def update_db(self):
        stat_for_refresh = []
        for file_id, (exch_obj, stat_obj) in self.files.iteritems():
            #:type : int, (ExchangeObject, bool, SaveStat)
            for stat_item in exch_obj.snd_statistics:
                send_id = stat_item['send_id']
                if stat_item['type'] == 'bo_send':
                    total_spend_time = self.tb_bosend._get_record_by_column_values('id', send_id, 'sent_time')[0][0]
                    block_time = stat_item['block_time']
                    try:
                        self.tb_bosend.edit('id', send_id,
                                            ('tries', 'sent', 'speed', 'sent_time', 'percent_done_float'),
                                            (exch_obj.bo_snd_status[send_id]['trynum'],
                                             stat_item['sent'],
                                             stat_item['speed'],
                                             block_time + total_spend_time,
                                             str(stat_item['percent_done'])), commit=False)

                        self.tb_sendstat.add(send_id, stat_item['block_size'], block_time, commit=False)
                    except KeyError:
                        pass
                elif stat_item['type'] == 'bo_upd':
                    info_item = exch_obj.bo_snd_status[send_id]
                    locked = 1 if info_item['locked'] else 0
                    tries = info_item['trynum']
                    tries_pos = info_item['tries_pos']
                    restart = 1 if info_item['restart'] else 0
                    stage = info_item['stage']
                    curr_error = info_item['curr_error']
                    self.tb_bosend.edit('id', send_id,
                                        ('tries', 'tries_pos', 'restart', 'locked', 'stage', 'curr_error'),
                                        (tries, tries_pos, restart, locked, stage, curr_error),
                                        commit=False)

                elif stat_item['type'] == 'bo_fin':
                    try:
                        self.tb_bosend.edit('id', send_id, ('percent_done_float',), (u'100',),
                                            commit=False)
                    except KeyError:
                        pass
                elif stat_item['type'] == 'remove_possend':
                    self.tb_possend.del_by_bosend(send_id, commit=False)

                elif stat_item['type'] == 'pos':
                    sendinfo = exch_obj.pos_snd_status[send_id]
                    posinfo = sendinfo['pos_info']
                    self.tb_bosend.edit('id', send_id, ('tries_pos',), (sendinfo['trynum'],),
                                        commit=False)

                    for pos_num, item in posinfo.iteritems():
                        if item['ping']:
                            accessibility = 1
                        else:
                            accessibility = 0
                        written_POS = self.tb_possend.edit(('bosend_id', 'posnum'),
                                                           (send_id, pos_num),
                                                           ('pos_status', 'posip', 'stage', 'accessible', 'timestamp',
                                                            'curr_error'),
                                                           (item['status'], item['ip'], item['stage'], accessibility,
                                                            item['stamp'], item['curr_error']),
                                                           commit=False)

                        if written_POS == 0:
                            self.tb_possend.add(send_id, pos_num, item['ip'], item['stage'], commit=False)

            log_write = {}
            for send_id, msg_type, message, timestamp in exch_obj.message_table:
                if type(send_id) != int:
                    raise TypeError(u'Unsupported send_id type')
                self.tb_messages.add(send_id, msg_type, message, timestamp)
                if send_id not in log_write:
                    log_write[send_id] = []
                log_write[send_id].append((msg_type, message, timestamp))

            for send_id, messages in log_write.iteritems():
                hostname = exch_obj.bo_snd_status[send_id]['hostname']

                write_lines = []
                for msg_type, message, timestamp in messages:
                    timestr = datetime.datetime.fromtimestamp(timestamp).strftime(u'%d.%m.%Y %H:%M:%S')
                    print u'{} {} {}'.format(timestr, msg_type, message)
                    write_lines.append(u'{} {} {}'.format(timestr, msg_type, message))
                save_log(self.log_dir, hostname, self.log_max_lines, write_lines)

            if len(exch_obj.snd_statistics) > 0:
                stat_for_refresh.append(stat_obj)
            exch_obj.message_table = []
            exch_obj.snd_statistics = []
        self.connection.commit()
        for stat_obj in stat_for_refresh:
            stat_obj.process_all()

    def process(self):
        #write_gc()
        self.fill_files()
        self.fill_send_params()

        for file_id, (exch_obj, stat_obj) in self.files.iteritems():
            stop_stage = exch_obj.post_proc['stop']

            for send_id, send_status in exch_obj.bo_snd_status.iteritems():
                if send_status['stage'] == 0:
                    self.qmanager.add_to_queue((send_id, exch_obj))
                elif  0 < send_status['stage'] < stop_stage or not send_status['checked']:
                    send_status['checked'] = True
                    self.qmanager.add_to_queue((send_id, exch_obj))
            stat_obj.process_all()
        while not self.qmanager.is_finished():
            print u'Активных задач в очереди {}'.format(self.qmanager.in_process)
            self.check_time_permission()
            self.tb_params.update_lock()
            #bo_finished, bo_errors = \
            self.qmanager.refresh_state()
            # self.qmanager_PP.refresh_state()
            #
            # for item in bo_finished:
            #     eo = item[1]
            #     if eo.post_proc:
            #         send_status = eo.bo_snd_status[item[0]]
            #         self.qmanager_PP.add_to_queue(item)
            #         send_status['checked'] = True
            self.update_db()
            time.sleep(self.refresh_time)
        self.qmanager.join(0)
        # self.qmanager_PP.join(0)
        print u'All tb_tasks have been successfully transferred'


def runner(connection):
    my_runner = Runner(connection)
    my_runner.process()