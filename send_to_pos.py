#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 31.07.13, 9:28
Useful description of the module ''
"""
__author__ = 'Taras.Taturevich'
import cx_Oracle
import time
from os import path
from exceptions import Exception
import socket

import x5
import myoracle
import file_transfer as ft


from file_transfer import PosTransferError
from file_transfer import remote_transfer as pos_transfer


class RollBack(Exception):
    def __init__(self, value):
        self.value = value


def get_pos_info(send_id, hostname, eo, restart):
    """
        send_id - id передачи файла;
        hostname - название BO
        restart = Требуется ли переотправка файлов на кассы (если перекачивался файл)
        eo - объект ExchangeObject
        Соединяемся с базой BO и получаем информацию по кассам,
        Если есть предыдущая информация по кассе в БД, то берем
        ее только в случае, если файл на эту кассу уже записан.

        :type send_id: int
        :type hostname: str or unicode
        :type eo: task_objects.TaskObject
        :param eo: Send object data
    """
    if eo.addrs_style == 'bonum' and path.exists('bo_subst.txt'):
        fake_hosts = dict(x.replace('\n', '').split(' ') for x in open('bo_subst.txt').read().strip().split('\r'))
        bo_ip = fake_hosts[hostname[3:]]
    else:
        bo_ip = x5.get_boip(hostname)

    pos_exclusions = eo.pos_exclusions
    bo_nonstandart = eo.bo_nonstandart

    try:
        ora_conn = myoracle.connect_db_timeout(bo_ip, eo.ora_port, eo.ora_sid, eo.ora_login,
                                               eo.ora_pwd, eo.ora_conn_timeout)
    except cx_Oracle.DatabaseError:
        print u'Couldn\'t connect to db of {}'.format(hostname)
        raise
    try:
        # noinspection PyUnboundLocalVariable
        pos_info_draft, start_pos_ipnum = x5.get_pos_info(ora_conn, hostname[3:], bo_ip, bo_nonstandart)
        # noinspection PyUnboundLocalVariable
        print u'Shop {}, POS status'.format(hostname)
        print '\n'.join((', '.join(unicode(x) for x in y) for y in pos_info_draft))

    finally:
        ora_conn.close()
        del ora_conn

    pos_info = {}

    pos_info_prev = eo.pos_snd_status[send_id]['pos_info']

    pos_info_draft_dict = dict(zip((x[0] for x in pos_info_draft), (x[1:] for x in pos_info_draft)))

    if len(pos_info_draft_dict):
        max_num = max(pos_info_draft_dict.keys())
    else:
        max_num = 15
    if max_num < 15:
        max_num = 15
        #raise x5.ShopError(u'Не обнаружено касс в магазине')
    dotpos = bo_ip.rfind('.')
    pos_ip_genpart = bo_ip[:dotpos + 1]
    #pos_ip_zeronum = int(pos_info_draft[0][4][dotpos + 1:]) - pos_info_draft[0][0]

    #for pos_num, pos_status, cashier, pos_stamp, pos_ip in pos_info_draft:
    for pos_num in xrange(1, max_num + 1):
        try:
            pos_status, cashier, pos_stamp, pos_ip = pos_info_draft_dict[pos_num]
            pos_affirmed = True
        except KeyError:
            pos_status = u'Неизвестен'
            pos_ip = pos_ip_genpart + str(start_pos_ipnum + pos_num - 1)
            pos_stamp = time.time()
            pos_affirmed = False
        try:
            info_prev_item = pos_info_prev[pos_num]
            stage = info_prev_item['stage']
            #restart = info_prev_item['restart']
        except KeyError:
            #info_item = {}
            stage = 0
            #restart = 0

        ping_res = x5.ping(pos_ip)
        if (not pos_affirmed and not ping_res) or pos_ip in pos_exclusions:
            continue

        if stage > 0 and not restart:
            info_item = pos_info_prev[pos_num]
        else:
            info_item = {'stage': 0}

        info_item['status'] = pos_status
        info_item['ip'] = pos_ip
        info_item['ping'] = ping_res
        if type(pos_stamp) == float:
            info_item['stamp'] = pos_stamp
        else:
            info_item['stamp'] = time.mktime(pos_stamp.timetuple())
        info_item['curr_error'] = ''
        pos_info[pos_num] = info_item

    eo.pos_snd_status[send_id]['pos_info'] = pos_info
    eo.add_update_req(send_id, 'pos')
    return pos_info


# def pos_transfer(ssh_conn, pos_ip, pos_login, pos_pwd, bo_path, pos_path, max_send_time=300):
#     pos_dir, pos_file = path.split(pos_path)
#     transport = ssh_conn.get_transport()
#     channel = transport.open_session()
#     channel.get_pty()
#     channel.settimeout(15)
#     channel.exec_command('sftp -o StrictHostKeyChecking=no {}@{}'.format(pos_login, pos_ip))
#     time.sleep(2)
#     channel.send('{}\r\n'.format(pos_pwd))
#     time.sleep(2)
#     curr_dir = ''
#     for dir_ in pos_dir.replace('\\', '/').strip('/').split('/'):
#         curr_dir += '/' + dir_
#         channel.send('mkdir {}\r\n'.format(curr_dir))
#         time.sleep(2)
#     if channel.recv_ready():
#         channel.recv(2048)
#     channel.send('put {} {}\r\n'.format(bo_path, pos_path))
#     #err_msg = None
#     time1 = time.time()
#     try:
#         while True:
#             if time.time() > time1 + max_send_time + 10:
#                 err_msg = u'Файл не был отправлен на кассу в течение отведенного времени'
#                 raise PosTransferError(err_msg)
#
#             if channel.recv_stderr_ready():
#                 err_msg = channel.recv_stderr(2048)
#                 raise PosTransferError(err_msg)
#
#             if channel.recv_ready():
#                 recv = channel.recv(2048)
#                 print recv
#                 if '100%' in recv:
#                     return True
#     finally:
#         channel.send('exit')
#         channel.close()
#         transport.close()


class PosSendError(Exception):
    def __ini__(self, value):
        self.value = value


class FileAccessError(Exception):
    def __ini__(self, value):
        self.value = value


class BODBError(Exception):
    def __ini__(self, value):
        self.value = value


class CommandExecError(Exception):
    def __ini__(self, value):
        self.value = value


def update_pos_info(eo, send_id):
    """
    :type eo: task_objects.TaskObject
    :type send_id: int
    """

    snd_status = eo.pos_snd_status[send_id]
    hostname = snd_status['hostname']
    try:
        #Получаем информацию по кассам в виде словаря
        #Ключи словаря: status, stamp, ip, stage, ping, restart
        pos_info = get_pos_info(send_id, hostname, eo, snd_status['restart'])
        snd_status['restart'] = False
        return pos_info

    except cx_Oracle.DatabaseError:
        raise BODBError


def process_all_pos(eo, send_id, fin_stage, func, params):
    """
    :type eo: task_objects.TaskObject
    :type send_id: int
    :type fin_stage: int
    :type func: function
    :type params: tuple or list
    """
    pos_login = eo.pos_login
    pos_pwd = eo.pos_pwd
    # pos_path = eo.pospath
    # res_path = eo.deflatepath
    snd_status = eo.pos_snd_status[send_id]

    pos_info = snd_status['pos_info']
    rollback = False
    rollback_msg = None

    for pos_num, info_item in pos_info.iteritems():
        info_item['curr_error'] = ''
        if info_item['ping'] and info_item['stage'] == fin_stage - 1:
            try:
                print u' Обрабатываю кассу {}'.format(pos_num)
                ssh_obj = ft.ssh_connect(info_item['ip'], 22, pos_login, pos_pwd, 5, 15)
                func(ssh_obj, *params)
                #deflate(ssh_obj, pos_path, res_path)
                #pos_transfer(ssh_conn, info_item['ip'], pos_login, pos_pwd, bo_path, pos_path, pos_send_time)
                info_item['stage'] = fin_stage
                eo.add_update_req(send_id, 'pos')
            except (ft.SSHException, socket.timeout, socket.error):
                rollback_msg = u'Couldn\'t connect to address {}'.format(info_item['ip'])
                eo.add_message(send_id, 'error', rollback_msg)
                print  info_item['ip'] + ' ' + rollback_msg
                info_item['curr_error'] = u'Не удалось подключиться к кассе'
                eo.add_update_req(send_id, 'pos')
                # eo.add_update_req(send_id, 'pos')
                continue
            except CommandExecError as r:
                rollback_msg = u'Не удалось выполнить команду {}'.format(func.func_name)
                info_item['curr_error'] = rollback_msg
                eo.add_update_req(send_id, 'pos')
                eo.add_message(send_id, 'error', r.message)
                info_item['stage'] -= 1
                rollback = True

            except FileAccessError:
                rollback_msg = u'Нет доступа к файлу на кассе {}'.format(pos_num)
                info_item['curr_error'] = rollback_msg
                info_item['stage'] -= 1
                eo.add_message(send_id, 'error', rollback_msg)
                eo.add_update_req(send_id, 'pos')
                rollback = True
            finally:
                try:
                    ssh_obj.close()
                    del ssh_obj
                except UnboundLocalError:
                    pass

    if rollback:
        raise RollBack(rollback_msg)

    if min(x['stage'] for x in pos_info.itervalues()) >= fin_stage:
        snd_status['stage'] = fin_stage
        #eo.snd_statistics.append({'send_id': send_id, 'type': 'pos_fin'})
        eo.add_update_req(send_id, 'pos')
        return True
    return False


def send_to_all_pos(hostname, eo, send_id, fin_stage):
    """
    :type eo: task_objects.TaskObject
    :type send_id: int
    """
    pos_login = eo.pos_login
    pos_pwd = eo.pos_pwd
    bo_path = eo.remotepath
    pos_path = eo.pospath
    bo_snd_status = eo.bo_snd_status[send_id]
    snd_status = eo.pos_snd_status[send_id]
    pos_send_time = eo.postime

    pos_info = eo.pos_snd_status[send_id]['pos_info']

    for pos_num, info_item in pos_info.iteritems():
        info_item['curr_error'] = ''
        if info_item['ping'] and info_item['stage'] < fin_stage:
            try:
                ssh_obj = ft.ssh_connect(hostname, 22, bo_snd_status['login'], bo_snd_status['password'], 5, 15)
                raise_on_file_notaccessible(ssh_obj, bo_path)
                pos_transfer(ssh_obj, info_item['ip'], pos_login, pos_pwd, bo_path, pos_path, pos_send_time)
                info_item['stage'] = fin_stage
                eo.add_update_req(send_id, 'pos')
            except PosTransferError as r:
                eo.add_message(send_id, 'error', r.message)
                info_item['curr_error'] = u'Не удалось отправить файл на кассу'
                eo.add_update_req(send_id, 'pos')
            except (ft.SSHException, socket.timeout, socket.error):
                msg = u'Couldn\'t connect to address {}'.format(info_item['ip'])
                eo.add_message(send_id, 'error', msg)
                print  hostname + ' ' + msg
                info_item['curr_error'] = u'Не удалось подключиться с магазина к кассе'
                eo.add_update_req(send_id, 'pos')
                # eo.add_update_req(send_id, 'pos')
                continue
            except Exception as r:
                print r.message
                info_item['curr_error'] = r.message
                eo.add_update_req(send_id, 'pos')
                continue
            finally:
                try:
                    ssh_obj.close()
                    del ssh_obj
                except UnboundLocalError:
                    pass

    if min(x['stage'] for x in pos_info.itervalues()) >= fin_stage:
        snd_status['stage'] = fin_stage
        #eo.snd_statistics.append({'send_id': send_id, 'type': 'pos_fin'})
        eo.add_update_req(send_id, 'pos')
        return True
    return False


def raise_on_file_notaccessible(ssh_conn, path):
    """
    :type ssh_conn: paramiko.SSHClient
    :type path: str or unicode
    """

    stdout, stderr = ssh_conn.exec_command('ls {}'.format(path))[1:]
    outstr = stdout.read()
    errstr = stderr.read()
    if not outstr and errstr.find('cannot access') > 0:
        raise FileAccessError(u'Cannot access path {}'.format(path))


def deflate(ssh_conn, arch_path, res_path):
    """
    :type ssh_conn: paramiko.SSHClient
    :type arch_path: str or unicode
    :type res_path: str or unicode
    """
    raise_on_file_notaccessible(ssh_conn, arch_path)
    stdout, stderr = ssh_conn.exec_command(u'mkdir -p {}'.format(res_path))[1:]
    outstr = stdout.read()
    errstr = stderr.read()
    if not outstr and len(errstr) > 0:
        raise CommandExecError(errstr)

    stdout, stderr = ssh_conn.exec_command('tar xvf {} -C {}'.format(arch_path, res_path))[1:]
    stdout.read()
    errstr = stderr.read()
    if len(errstr) > 0:
        raise CommandExecError(errstr)
    return True


def run_script(ssh_conn, script_path):
    """
    :type ssh_conn: paramiko.SSHClient
    :type script_path: str or unicode
    """
    #script_dir = path.dirname(script_path)
    raise_on_file_notaccessible(ssh_conn, script_path)

    scr_dir, scr_file = path.split(script_path)

    stdout, stderr = ssh_conn.exec_command('cd {}; ./{}'.format(str(scr_dir), str(scr_file)))[1:]
    outstr = stdout.read()
    print outstr
    errstr = stderr.read()
    print errstr
    if not outstr and len(errstr) > 0:
        raise CommandExecError(errstr)
    return True


def post_production(send_id, eo):
    """
    :type send_id: int
    :type eo: task_objects.TaskObject
    """
    bo_send_status = eo.bo_snd_status[send_id]
    snd_status = eo.pos_snd_status[send_id]
    #::type :int
    trynum = bo_send_status['tries_pos'] + 1
    bo_send_status['tries_pos'] = trynum
    #::type :str or unicode
    hostname = snd_status['hostname']
    if eo.addrs_style == 'bonum' and path.exists('host_sim.txt'):
        fake_hosts = dict(x.replace('\n', '').split(' ') for x in open('bo_subst.txt').read().strip().split('\r'))
        hostname = fake_hosts[hostname[3:]]


    bo_send_status['curr_error'] = ''
    eo.add_update_req(send_id, 'pos')
    eo.add_update_req(send_id, 'bo_upd')
    local_path = eo.localpath
    bo_path = eo.remotepath
    pos_path = eo.pospath
    stage = bo_send_status['stage']
    movetopos = eo.post_proc['movetopos']

    try:
        #Check if file available in the BO and if not: restart uploading
        ssh_obj = ft.ssh_connect(hostname, 22, bo_send_status['login'], bo_send_status['password'], 5, 15)
    except (ft.SSHException, socket.timeout, socket.error):
        msg = u'Couldn\'t connect to BO {}'.format(hostname)
        eo.add_message(send_id, 'error', msg)
        print  hostname + ' ' + msg
        bo_send_status['curr_error'] = u'Не удалось подключиться к серверу'
        return False

    try:
        if movetopos > 0:
            try:
                pos_info = update_pos_info(eo, send_id)
                if len(pos_info) == 0:
                    msg = u'{} - не удалось обнаружить кассы в магазине'.format(hostname)
                    print msg
                    eo.add_message(send_id, 'error', msg)
                    bo_send_status['curr_error'] = msg
                    return False

                pos_min_stage = min(x[1]['stage'] for x in pos_info.iteritems())
                if pos_min_stage < stage - 1:
                    stage = pos_min_stage + 1
                    bo_send_status['stage'] = stage
                    eo.add_update_req(send_id, 'bo_upd')
            except BODBError:
                msg = u'{} - Нет доступа к БД магазина'.format(hostname)
                print msg
                eo.add_message(send_id, 'error', msg)
                bo_send_status['curr_error'] = msg
                return False
            except x5.ShopError as r:
                eo.add_message(send_id, 'error', r.value)
                print r.value
                bo_send_status['curr_error'] = r.value
                return False
        else:
            pos_info = {}

        if stage == movetopos:
            print u'Распространяю файл {} по кассам BO {}'.format(local_path, hostname)
            print 'try # {}'.format(trynum)

            try:
                if send_to_all_pos(hostname, eo, send_id, movetopos):
                    stage += 1
                    bo_send_status['stage'] = stage
                    eo.add_update_req(send_id, 'bo_upd')
            except FileAccessError:
                bo_send_status['restart'] = True
                bo_send_status['stage'] = 0
                bo_send_status['curr_error'] = u'Нет доступа к файлу на БО, переотправляю файл'
                eo.add_update_req(send_id, 'bo_upd')
                return False
            except x5.ShopError as r:
                print r.value
                eo.add_message(send_id, 'error', r.value)
                bo_send_status['curr_error'] = r.value
                eo.add_update_req(send_id, 'bo_upd')
                return False

        deflate_stage = eo.post_proc['deflate']

        if any(True for x in pos_info.itervalues() if x['stage'] == deflate_stage - 1) \
                or stage == deflate_stage:
            print u'Распаковываю файл {} на {}'.format(local_path, hostname)
            try:
                if 0 < movetopos < deflate_stage:
                    try:
                        if process_all_pos(eo, send_id, deflate_stage, deflate, (pos_path, eo.deflatepath)):
                            stage = deflate_stage + 1
                            bo_send_status['stage'] = stage
                            eo.add_update_req(send_id, 'bo_upd')
                    except RollBack as r:
                        if stage == deflate_stage:
                            stage -= 1
                            bo_send_status['stage'] = stage
                        bo_send_status['curr_error'] = r.value
                        eo.add_message(send_id, 'error', r.value)
                        eo.add_update_req(send_id, 'bo_upd')
                        return False
                else:
                    try:
                        if deflate(ssh_obj, bo_path, eo.deflatepath):
                            stage = deflate_stage + 1
                            bo_send_status['stage'] = stage
                            eo.add_update_req(send_id, 'bo_upd')
                    except FileAccessError:
                        bo_send_status['restart'] = True
                        bo_send_status['stage'] = 0
                        bo_send_status['curr_error'] = u'Нет доступа к файлу на БО, переотправляю файл'
                        eo.add_update_req(send_id, 'bo_upd')
                        return False
            except CommandExecError as r:
                try:
                    err_message = r.value.decode('utf-8', errors='ignore')
                except AttributeError:
                    err_message = r.message.decode('utf-8', errors='ignore')
                print err_message
                eo.add_message(send_id, u'error', err_message)
                bo_send_status['curr_error'] = err_message
                eo.add_update_req(send_id, u'bo_upd')
                return False

        script_stage = eo.post_proc['runscript']

        if any([True for x in pos_info.itervalues() if x['stage'] == script_stage - 1]) \
                or (stage == script_stage):
            try:
                print u'Выполняю скрипт {} на {}'.format(eo.scriptpath, hostname)
                if 0 < movetopos < script_stage:
                    try:
                        if process_all_pos(eo, send_id, script_stage, run_script, (eo.scriptpath,)):
                            stage = script_stage + 1
                            bo_send_status['stage'] = stage
                            eo.add_update_req(send_id, 'bo_upd')
                    except RollBack as r:
                        if stage == script_stage:
                            stage -= 1
                            bo_send_status['stage'] = stage
                        bo_send_status['curr_error'] = r.value
                        eo.add_message(send_id, 'error', r.value)
                        eo.add_update_req(send_id, 'bo_upd')
                        return False
                else:
                    if run_script(ssh_obj, eo.scriptpath):
                        stage = script_stage + 1
                        bo_send_status['stage'] = stage
                        eo.add_update_req(send_id, 'bo_upd')
            except FileAccessError:
                bo_send_status['restart'] = True
                bo_send_status['stage'] = 0
                bo_send_status['curr_error'] = u'Недоступен файл , перезагружаю'
                return False
            except CommandExecError as r:
                try:
                    err_message = r.value.decode('utf-8', errors='ignore')
                except AttributeError:
                    err_message = r.message.decode('utf-8', errors='ignore')
                print err_message
                eo.add_message(send_id, 'error', err_message)
                bo_send_status['curr_error'] = u'Не удалось выполнить команду, {}'.format(r.message)
                return False
        bo_send_status['curr_error'] = ''
    except (ft.SSHException, socket.timeout, socket.error):
        msg = u'Lost connection to the BO {}'.format(hostname)
        eo.add_message(send_id, 'error', msg)
        print  hostname + ' ' + msg
        bo_send_status['curr_error'] = u'Потеряно соединение с сервером'
        # eo.add_update_req(send_id, 'pos')
        return False
    finally:
        bo_send_status['locked'] = False
        bo_send_status['send_stamp'] = time.time()
        eo.add_update_req(send_id, 'bo_upd')
        try:
            ssh_obj.close()
            del ssh_obj
        except UnboundLocalError:
            pass