#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 20.08.13, 12:50
Useful description of the module ''
"""
__author__ = 'Taras.Taturevich'
import traceback
from threading import Thread
from Queue import Queue
import time


class ManageQueue(object):
    """
    Class for controlling self managed queue of tasks.
    You should only define data for queue items and function for their processing
    Processing function decides if its task is already finished or it should be done later
    If task is done it should return True, otherwise - False
    Finished tasks data is returned in refresh_state function
    If task is not finished after first (n-st) run, it would be returned to queue after defined timeout
    The class is intended for tasks dependent from external resources (e.g. sending tasks to different hosts)
    which completion sometimes could not be performed from first time.

    Queue created when start_queues is called. To add task to queue use add_to_queue. Run refresh_state
    from time to time to get finished tasks and refresh queue state (return to queue tasks from rest)


    Typical run sequence:  init -> start_queues -> add_to_queue -> refresh_state -> join -> refresh_state
    """
    def __init__(self, thread_num, proc_func, rest_timeout=1, verbose=False):
        """
        :param int thread_num: number of threads in pool
        :param function proc_func: function for processing
        :param float or int rest_timeout: minimum timeout for task before returning to main queue
        :param bool verbose: if True, prints exception messages from subthreads
        :param exchange_object: object with additional data for tasks and for storing return data from tasks
        """

        self.thread_num = thread_num
        #::type :Queue
        self.__queue = None
        self.proc_func = proc_func

        #task_id, task_data, task_state (wait, locked, finished
        #::type: list
        self.last_id = 0
        self.timeout = rest_timeout
        #data, timestamp
        self.tasks_data = {}
        self.wait_queue = []
        self.finished_queue = []
        self.verbose = verbose

        self.error_msgs = []
        self.in_process = 0

    def start_queues(self):
        """
        Create queue
        """
        self.__queue = Queue()

        for i in range(self.thread_num):
            worker = Thread(target=self.__process, args=(self.__queue,))
            worker.setDaemon(True)
            worker.start()

    def _add_one_task_to_queue(self, data, timestamp):
        self.in_process += 1
        task_id = self.last_id
        self.last_id += 1
        self.tasks_data[task_id] = [data, timestamp]
        self.__queue.put(task_id)
        #self.wait_queue.append(task_id)

    def add_to_queue(self, data, multi=False):
        """
        Add new task to queue

        :param data: task data (or list of tasks if multi)
        :param bool multi: if multi care data as list of tasks data
        """
        timestamp = time.time()
        if multi:
            for item in data:
                self._add_one_task_to_queue(item, timestamp)
        else:
            self._add_one_task_to_queue(data, timestamp)

    def refresh_state(self):
        """
        Refreshes queue state
        Returns list data of finished tasks and list of errors occured

        :rtype: list, list
        """
        threshold_time = time.time() - self.timeout
        wait_queue2 = []
        for task_id in self.wait_queue:
            if threshold_time >= self.tasks_data[task_id][1]:
                self.__queue.put(task_id)
            else:
                wait_queue2.append(task_id)

        self.wait_queue = wait_queue2

        finished = self.finished_queue
        self.in_process -= len(finished)
        error_msgs = self.error_msgs
        self.finished_queue = []
        self.error_msgs = []
        return finished, error_msgs

    def __process(self, queue):
        while True:
            task_id = queue.get()
            finished = False
            data = self.tasks_data[task_id][0]
            # noinspection PyBroadException
            try:
                finished = self.proc_func(data)
            except Exception as r:
                self.error_msgs.append((data, r))
                #exc = traceback.format_exc()
                #self.error_msgs.append((data, exc))
                if self.verbose:
                    print u'task_id = {}'.format(task_id)
                    print traceback.format_exc()
                finished = False
            else:
                if finished:
                    self.finished_queue.append(data)
                    #finished = True

            if not finished:
                self.tasks_data[task_id][1] = time.time()
                self.wait_queue.append(task_id)
            queue.task_done()

    def join(self, refresh_time=1):
        """
        Join to queue
        """
        finished = []
        errors = []
        while not self.is_finished():
            curr_finished, curr_errors = self.refresh_state()
            finished.extend(curr_finished)
            errors.extend(errors)
            time.sleep(refresh_time)
        self.__queue.join()
        return finished, errors

    def is_finished(self):
        if self.in_process==0:
            return True
        else:
            return False

    def stop_task(self, data):
        pass





def main():
    pass


if __name__ == "__main__":
    main()
