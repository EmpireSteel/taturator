#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 19.07.13, 13:58
Useful description of the module ''
"""
__author__ = 'Taras.Taturevich'

import paramiko
import time
import os
import hashlib
#import tempfile
from os import path
import traceback
import socket

SSHException = paramiko.SSHException


class PosTransferError(Exception):
    def __init__(self, value):
        self.value = value


def sha256sum_readable(fpath, blocksize=65536):
    hasher = hashlib.sha256()
    with open(fpath, 'rb') as f:
        buf = f.read(blocksize)
        while len(buf) > 0:
            hasher.update(buf)
            buf = f.read(blocksize)
    digest_lst = []
    for symb in hasher.digest():
        hex_num = hex(ord(symb)).replace('0x', '')
        if len(hex_num) == 1:
            hex_num = '0' + hex_num
        digest_lst.append(hex_num)

    return ''.join(digest_lst)


def  putfo_resume(SFTP_conn, localpath, remotepath, blocksize=65536, callback=None, pause_func=None):
    """
    callback:
        'bytes_total':lfile_size,
        'bytes_2sent':lfile_size - start_pos,
        'bytes_sent':loaded,
        'bytes_last_block':lenbuf,
        'time_total':curr_time - time_st,
        'time_last_block': curr_time - prev_time

    pause_func - when returns False, uploading should be paused
    pause start to work only when block is loaded

    """
    time_st = time.clock()
    remotefo = SFTP_conn.open(remotepath, 'a')

    remoteparam = remotefo.stat()
    start_pos = remoteparam.st_size
    lfile_size = os.path.getsize(localpath)
    with open(localpath, 'rb') as localfo:
        localfo.seek(start_pos)
        buf = localfo.read(blocksize)
        pos = start_pos
        loaded = 0
        curr_time = time_st
        while buf:
            remotefo.write(buf)
            buf = localfo.read(blocksize)
            pos += blocksize
            lenbuf = len(buf)
            loaded += lenbuf
            prev_time = curr_time
            curr_time = time.clock()
            if callback:
                #print 'time_last_block {}'.format(curr_time - prev_time)
                #print 'prev_time {}'.format(prev_time)
                #print 'curr_time {}'.format(curr_time)
                load_info = {
                    'bytes_total': lfile_size,
                    'bytes_startpos': start_pos,
                    'bytes_sent': loaded,
                    'bytes_last_block': lenbuf,
                    'time_total': curr_time - time_st,
                    'time_last_block': curr_time - prev_time
                }
                callback(load_info)
            if pause_func:
                while not pause_func():
                    time.sleep(10)
    remotefo.close()
    return time.time() - time_st, pos, lfile_size


def  getfo_resume(SFTP_conn, localpath, remotepath, blocksize=65536, callback=None, pause_func=None):
    """
    callback:
        'bytes_total':lfile_size,
        'bytes_2sent':lfile_size - start_pos,
        'bytes_sent':loaded,
        'bytes_last_block':lenbuf,
        'time_total':curr_time - time_st,
        'time_last_block': curr_time - prev_time

    pause_func - when returns False, uploading should be paused
    pause start to work only when block is loaded

    """
    time_st = time.clock()
    remotefo = SFTP_conn.open(remotepath, 'rb')
    remoteparam = remotefo.stat()
    rfile_size = remoteparam.st_size

    #start_pos = remoteparam.st_size
    try:
        start_pos = os.path.getsize(localpath)
    except WindowsError:
        start_pos = 0
    with open(localpath, 'a') as localfo:
        remotefo.seek(start_pos)
        buf = remotefo.read(blocksize)
        pos = start_pos
        loaded = 0
        curr_time = time_st
        while buf:
            localfo.write(buf)
            buf = remotefo.read(blocksize)
            pos += blocksize
            lenbuf = len(buf)
            loaded += lenbuf
            prev_time = curr_time
            curr_time = time.clock()
            if callback:
                #print 'time_last_block {}'.format(curr_time - prev_time)
                #print 'prev_time {}'.format(prev_time)
                #print 'curr_time {}'.format(curr_time)
                load_info = {
                    'bytes_total': rfile_size,
                    'bytes_startpos': start_pos,
                    'bytes_sent': loaded,
                    'bytes_last_block': lenbuf,
                    'time_total': curr_time - time_st,
                    'time_last_block': curr_time - prev_time
                }
                callback(load_info)
            if pause_func:
                while not pause_func():
                    time.sleep(10)
    remotefo.close()
    return time.time() - time_st, pos, rfile_size


def prnt_info(send_info):
    total_size = send_info['bytes_total']
    bytes_sent = send_info['bytes_sent'] + send_info['bytes_startpos']
    instant_speed = send_info['bytes_last_block'] / send_info['time_last_block'] / 1000
    percent = bytes_sent / float(total_size)
    print u'Sent {} Mb from {} Mb, {:.2%} with {:.2f} kBps'.format(bytes_sent/1024/1024, total_size/1024/1024, percent, instant_speed)


def ssh_connect(hostname, port, username, password, tries=1, pause=0):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    err_count = 0
    while True:
        #print 'connect, try {}'.format(i)
        try:
            ssh.connect(hostname=hostname, port=port, username=username, password=password)

            #ssh_obj = SSH_connect(hostname, port, username, password)
            return ssh
        except SSHException:

            if err_count < tries:
            #print r.message
                err_count += 1
                time.sleep(pause)
            else:
                raise
        except (socket.timeout, socket.error):
            #print r.message
            raise
        # except Exception as r:
        #     print r.message


def remote_transfer(ssh_conn, trg_ip, trg_login, trg_pwd, src_path, trg_path, max_send_time=300):
    """
    Transfer file  from remote host represented by connection ssh_conn to remote host trg_ip using sftp over ssh

    :param paramiko.SSHClient ssh_conn: SSH connection to source host
    :param str or unicode trg_ip: target ip
    :param str or unicode trg_login:  ssh login to target host
    :param str or unicode trg_pwd: ssh password to target host
    :param str or unicode src_path:path to file to be transfered on source host
    :param str or unicode trg_path: path to transfer file on target host
    :param int max_send_time: max time for sending file, if not, raise PosTransferError with corresponding message
    :return:
    """

    trg_dir, trg_file = path.split(trg_path)
    transport = ssh_conn.get_transport()
    channel = transport.open_session()
    channel.get_pty()
    channel.settimeout(15)
    channel.exec_command('sftp -o StrictHostKeyChecking=no {}@{}'.format(trg_login, trg_ip))
    time.sleep(2)
    channel.send('{}\r\n'.format(trg_pwd))
    time.sleep(2)
    curr_dir = ''
    for dir_ in trg_dir.replace('\\', '/').strip('/').split('/'):
        curr_dir += '/' + dir_
        channel.send('mkdir {}\r\n'.format(curr_dir))
        time.sleep(2)
    if channel.recv_ready():
        channel.recv(2048)
    channel.send('put {} {}\r\n'.format(src_path, trg_path))
    #err_msg = None
    time1 = time.time()
    try:
        while True:
            if time.time() > time1 + max_send_time + 10:
                err_msg = u'Файл не был отправлен на кассу в течение отведенного времени'
                raise PosTransferError(err_msg)

            if channel.recv_stderr_ready():
                err_msg = channel.recv_stderr(2048)
                raise PosTransferError(err_msg)

            if channel.recv_ready():
                recv = channel.recv(2048)
                print recv
                if '100%' in recv:
                    return True
    finally:
        channel.send('exit')
        channel.close()
        transport.close()


class RemoteInteraction(object):
    def __init__(self, hostname, port, username, password, localpath, remotepath, timeout=30, blocksize=1048576):
        self.hostname = hostname
        self.port = port
        self.username = username
        self.password = password
        self.localpath = localpath
        self.remotepath = remotepath
        self.timeout = timeout
        self.ssh = None
        self.sftp = None
        self.blocksize = blocksize
        self.err_message = None
        self.pause_func = None

    def set_pause_func(self, func):
        """
        Задаем функцию паузы. Пока функция возвращает False - приостанавливаем передачу
        """
        self.pause_func = func

    def connect(self, tries=1, pause=0):
        # try:
            # self.ssh = paramiko.SSHClient()
            # self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh = ssh_connect(self.hostname, self.port, self.username, self.password, tries, pause)
        self.sftp = self.ssh.open_sftp()
        channel = self.sftp.get_channel()
        channel.settimeout(self.timeout)
        #     return True
        # except SSHException as r:
        #     #print u'Could not connect to {} server, trying again'.format(host)
        #     self.err_message = r.message
        #     return False

    def close(self):
        try:
            self.sftp.close()
            self.ssh.close()
            return True
        except AttributeError:
            return False

    def send(self, callback=None):
        putfo_resume(self.sftp, self.localpath, self.remotepath,
                     callback=callback, blocksize=self.blocksize, pause_func=self.pause_func)

    def receive(self, callback=None):
        getfo_resume(self.sftp, self.localpath, self.remotepath,
                     callback=callback, blocksize=self.blocksize, pause_func=self.pause_func)

    def verify(self):
        hash_ = sha256sum_readable(self.localpath)
        hashpath = self.remotepath.strip() + '_hash'
        remotedir, remotefilename = path.split(self.remotepath)
        remotefo = self.sftp.open(str(hashpath), 'w')
        remotefo.write(hash_ + '  ' + remotefilename)
        remotefo.close()
        stdin, stdout, stderr = self.ssh.exec_command('cd "{}"; sha256sum -c "{}"'.format(remotedir,
                                                                                          path.basename(hashpath)))
        err = stderr.read().strip('\n')
        self.ssh.exec_command('rm "{}"'.format(hashpath))

        if err == '':
            return True
        else:
            return False

    def remove(self):
        self.ssh.exec_command('rm "{}"'.format(self.remotepath))

def main():
    #time.sleep(14400)

    port = 22
    password = "1"                #hard-coded
    username = "mgmgkappl"                #hard-coded
    remotepath = '/home/mgmgkappl/tst/testoo.tar.gz'
    localpath = 'testoo.tar.gz '

    test_hosts = ['5113', '7609', '8416',  '5140', '6966', '5043', '6961', '7057', '6970', '6801']

    correct = False
    for host in test_hosts:
        print 'host {}'.format(host)
        num = 0
        correct = False
        while not correct:
            num += 1
            print 'try # {}'.format(num)
            try:
                send_obj = RemoteInteraction('bo-{}'.format(host), port, username,
                                             password, localpath, remotepath, blocksize=1048576)

                connected = False
                for i in xrange(20):
                    print 'connect, try {}'.format(i)
                    if send_obj.connect():
                        connected = True
                        break
                    else:
                        print send_obj.err_message
                        time.sleep(20)
                if not connected:
                    break

                send_obj.ssh.exec_command('mkdir /home/mgmgkappl/tst/')
                print 'send file'
                send_obj.send(callback=prnt_info)
                print 'verify'
                if send_obj.verify():
                    print u'File is succesfully uploaded'
                    correct = True
                else:
                    print u'Uploaded file is incorrect'
                    send_obj.remove()
                    time.sleep(300)
            except:
                traceback.print_exc(file=open("err.log", "a"))

            finally:
                send_obj.close()

def main2():
    from os import path
    conn = ssh_connect('10.152.152.131', 22, 'root', '113525')
    login = 'root'
    host = '10.152.152.42'
    password = '1'
    pos_path = '/opt/aus/spool/NCRPatch'
    posfname = 'fonts.tar.gz'
    remotepath = '/home/mgmgkappl/FONTS/fonts.tar.gz'

    channel = conn.get_transport().open_session()
    channel.get_pty()
    channel.settimeout(5)
    channel.exec_command('sftp -o StrictHostKeyChecking=no {}@{}'.format(login, host))
    channel.send('{}\n'.format(password))
    #print channel.recv(1024)

    channel.send('mkdir {}\r\n'.format(pos_path))
    channel.setblocking(1)
    channel.send('put {} {}\r\n'.format(remotepath, pos_path + '/' + posfname))
    err_msg = None
    while True:
        if channel.recv_stderr_ready():
            err_msg = channel.recv_stder()
            break

        if channel.recv_ready():
            recv = channel.recv(2048)
            print recv
            if '100%' in recv:
                break

    if err_msg is not None:
        print 'something'

    channel.close()
    conn.close()

if __name__ == "__main__":
    main2()