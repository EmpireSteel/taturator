#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 28.08.13, 17:37
Useful description of the module ''
"""
__author__ = 'Taras.Taturevich'

import file_transfer as ft
import socket
import traceback
import sys
import multiqueue
import time

def prnt_info(send_info, hostname):
    total_size = send_info['bytes_total']
    bytes_sent = send_info['bytes_sent'] + send_info['bytes_startpos']
    instant_speed = send_info['bytes_last_block'] / send_info['time_last_block']
    percent = bytes_sent / float(total_size)
    print u'{} Received {} Kb from {} Kb, {:.2%} with {:.2f} kBps'.format(hostname, bytes_sent / 1024,
                                                                          total_size / 1024, percent,
                                                                          instant_speed / 1024)


def get_from_host(data):

    hostname, login, pwd, remotepath, localpath = data

    print 'Transfer file {} from {}'.format(remotepath, hostname)
    try:
        send_obj = ft.RemoteInteraction(hostname, 22, login, pwd, localpath,
                                        remotepath, 60, 65536)

        print 'Connecting to {} ...'.format(hostname)
        try:
            send_obj.connect(20, 15)
        except (ft.SSHException, socket.timeout, socket.error):
            print u'Ошибка соединения'
            return False

        print u'{} Получаю файл {}'.format(hostname, remotepath)

        my_print_info = lambda x: prnt_info(x, hostname)
        send_obj.receive(callback=my_print_info)
        # print '{} verify {}'.format(hostname, eo.localpath)
        # if send_obj.verify():
        txt = u'{} File "{}" is succesfully downloaded'.format(hostname, localpath)
        print txt
        return True
        # else:
        #     txt = u'{} Загруженный файл {} некорректен'.format(hostname, eo.localpath)
        #     print txt
        #     snd_status['curr_error'] = txt
        #     eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})
        #
        #     eo.add_message(send_id, 'error', txt)
        #     send_obj.remove()
        #     #del txt
    except Exception as r:
        if hasattr(r, 'errno') and r.errno == 10060:
            print u'Could not connect to {}'.format(hostname)
        elif type(r) == socket.timeout:
            print 'Socket timeout'
        else:
            print traceback.format_exc()
        return False

conf_name = sys.argv[1]

with open(conf_name, 'r') as f:
    params = [[y.strip(' ') for y in x.split('|')] for x in f.read().split('\n')]


queue = multiqueue.ManageQueue(10, get_from_host, verbose=True)
queue.start_queues()
queue.add_to_queue(params, multi=True)


while not queue.is_finished():
    finished = queue.refresh_state()
    time.sleep(20)

#get_from_host(host, login, pwd, remotepath, localpath)

