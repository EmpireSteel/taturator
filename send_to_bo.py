#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 31.07.13, 11:53
Useful description of the module ''
"""
__author__ = 'Taras.Taturevich'
# import time
import file_transfer as ft
from os import path
import traceback
import socket

ACTIVE_BO = {}


def prnt_info(send_info, send_id, hostname, eo):
    total_size = send_info['bytes_total']
    bytes_sent = send_info['bytes_sent'] + send_info['bytes_startpos']
    instant_speed = send_info['bytes_last_block'] / send_info['time_last_block']
    percent = bytes_sent / float(total_size)
    print u'{} Sent {} Kb from {} Kb, {:.2%} with {:.2f} kBps'.format(hostname, bytes_sent / 1024,
                                                                      total_size / 1024, percent,
                                                                      instant_speed / 1024)
    eo.snd_statistics.append(
        {'send_id': send_id,
         'sent': bytes_sent,
         'total': total_size,
         'speed': instant_speed,
         'block_time': send_info['time_last_block'],
         'percent_done': percent * 100,
         'block_size': send_info['bytes_last_block'],
         'type': 'bo_send'}
    )


def send_2bo(send_id, eo):
    """
    :type send_id: int
    :type eo:task_objects.TaskObject
    """
    snd_status = eo.bo_snd_status[send_id]
    trynum = snd_status['trynum'] + 1
    snd_status['trynum'] = trynum
    eo.update_db(send_id)
    print 'try # {}'.format(trynum)

    hostname = snd_status['hostname']

    if eo.addrs_style == 'bonum' and path.exists('bo_subst.txt'):
        fake_hosts = dict(x.replace('\n', '').split(' ') for x in open('bo_subst.txt').read().strip().split('\r'))
        hostname = fake_hosts[hostname[3:]]

    print u'Передаю файл {} на {}'.format(eo.localpath, hostname)

    if hostname in ACTIVE_BO and ACTIVE_BO[hostname]:
        print u'Уже есть активная закачка на магазин {}, жду когда освободится'.format(hostname)
        return False

    try:
        ACTIVE_BO[hostname] = True
        send_obj = ft.RemoteInteraction(hostname, 22, snd_status['login'], snd_status['password'], eo.localpath,
                                        eo.remotepath, eo.conntimeout, int(eo.blocksize))

        send_obj.pause_func = eo.check_enabled_state
        print 'Connecting to {} ...'.format(hostname)
        try:
            send_obj.connect(20, 15)
        except (ft.SSHException, socket.timeout, socket.error):
            snd_status['curr_error'] = u'Ошибка соединения'
            eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})

            #queue.task_done()
            return False
        else:
            snd_status['connect'] = True

        if snd_status['restart'] == 1:
            print u'{} Restart file'.format(hostname)
            send_obj.remove()
            snd_status['restart'] = 0
            eo.del_possend(send_id)
            eo.update_db(send_id)

        in_, out_, err_ = send_obj.ssh.exec_command('mkdir -p {}'.format(path.dirname(eo.remotepath)))
        msg = err_.read()
        if msg:
            eo.add_message(send_id, 'error', msg)
            print u'Не удалось создать каталог {}\n{}'.format(path.dirname(eo.remotepath), msg)
            return False

        print u'{} Отправляю файл {} на {}'.format(hostname, eo.localpath, eo.remotepath)

        my_print_info = lambda x: prnt_info(x, send_id, hostname, eo)
        try:
            send_obj.send(callback=my_print_info)
        except IOError as r:
            message = u'Не могу записать файл "{}", возникает ошибка "{}". Проверьте настройку пути remotepath'.format(eo.remotepath, r.message)
            print message
            eo.add_message(send_id, 'error', message)
            return False
        except ft.SSHException as r:
            message = u'Разорвано соединение с сервером {}'.format(hostname)
            print message
            eo.add_message(send_id, 'error', message)
            return False
        print '{} verify {}'.format(hostname, eo.localpath)
        if send_obj.verify():
            txt = u'{} File "{}" is succesfully uploaded'.format(hostname, eo.localpath)
            print txt
            eo.add_message(send_id, 'info', txt)
            snd_status['stage'] = 1
            eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_fin'})
            return True
        else:
            txt = u'{} Загруженный файл {} некорректен'.format(hostname, eo.localpath)
            print txt
            snd_status['curr_error'] = txt
            eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})
            eo.add_message(send_id, 'error', txt)
            send_obj.remove()
            return False
    except Exception as r:
        if hasattr(r, 'errno') and r.errno == 10060:
            print u'Could not connect to {}'.format(hostname)
            snd_status['curr_error'] = u'Не удалось соединиться'
            eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})
        elif type(r) == socket.timeout:
            msg = 'Socket timeout'
            eo.add_message(send_id, 'error', msg)
            snd_status['curr_error'] = u'Таймаут сокета'
            eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})

        else:
            eo.add_message(send_id, 'error', traceback.format_exc())
            snd_status['curr_error'] = u'Неизвестная ошибка'
            eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})

            print traceback.format_exc()
            return False
    finally:
        # noinspection PyUnboundLocalVariable
        ACTIVE_BO[hostname] = False
        try:
            send_obj.close()
        except Exception:
            pass

        #del trynum


# def send_2bo_multi(data):
#     """
#     :type data: (int, transfer_multi.ExchangeObject)
#     """
#     send_id, eo = data
#
#     while not eo.enabled_time:
#         time.sleep(10)
#
#     #::type : dict
#     snd_status = eo.bo_snd_status[send_id]
#     snd_status['locked'] = True
#     snd_status['curr_error'] = ''
#     trynum = snd_status['trynum'] + 1
#     snd_status['trynum'] = trynum
#     eo.update_db(send_id)
#     hostname = snd_status['hostname']
#
#     print 'Transfer file {} to {}'.format(eo.localpath, hostname)
#     print 'try # {}'.format(trynum)
#     try:
#         send_obj = ft.RemoteInteraction(hostname, 22, eo.login, eo.password, eo.localpath,
#                                         eo.remotepath, eo.conntimeout, int(eo.blocksize))
#
#         send_obj.pause_func = eo.check_enabled_state
#         print 'Connecting to {} ...'.format(hostname)
#         try:
#             send_obj.connect(20, 15)
#         except (ft.SSHException, socket.timeout, socket.error):
#             snd_status['curr_error'] = u'Ошибка соединения'
#             eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})
#
#             #queue.task_done()
#             return False
#         else:
#             snd_status['connect'] = True
#
#         if snd_status['restart'] == 1:
#             print u'{} Restart file'.format(hostname)
#             send_obj.remove()
#             snd_status['restart'] = 0
#             eo.del_possend(send_id)
#             eo.update_db(send_id)
#
#         in_, out_, err_ = send_obj.ssh.exec_command('mkdir -p {}'.format(path.dirname(eo.remotepath)))
#         msg = err_.read()
#         if msg:
#             eo.add_message(send_id, 'error', msg)
#             print u'Не удалось создать каталог {}\n{}'.format(path.dirname(eo.remotepath), msg)
#             return False
#
#         print '{} send file {}'.format(hostname, eo.localpath)
#
#         my_print_info = lambda x: prnt_info(x, send_id, hostname, eo)
#         send_obj.send(callback=my_print_info)
#         print '{} verify {}'.format(hostname, eo.localpath)
#         if send_obj.verify():
#             txt = u'{} File "{}" is succesfully uploaded'.format(hostname, eo.localpath)
#             print txt
#             eo.add_message(send_id, 'info', txt)
#             snd_status['stage'] = 1
#             eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_fin'})
#             return True
#         else:
#             txt = u'{} Загруженный файл {} некорректен'.format(hostname, eo.localpath)
#             print txt
#             snd_status['curr_error'] = txt
#             eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})
#             eo.add_message(send_id, 'error', txt)
#             send_obj.remove()
#             return False
#     except Exception as r:
#         if hasattr(r, 'errno') and r.errno == 10060:
#             print u'Could not connect to {}'.format(hostname)
#             snd_status['curr_error'] = u'Не удалось соединиться'
#             eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})
#         elif type(r) == socket.timeout:
#             msg = 'Socket timeout'
#             eo.add_message(send_id, 'error', msg)
#             snd_status['curr_error'] = u'Таймаут сокета'
#             eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})
#
#         else:
#             eo.add_message(send_id, 'error', traceback.format_exc())
#             snd_status['curr_error'] = u'Неизвестная ошибка'
#             eo.snd_statistics.append({'send_id': send_id, 'type': 'bo_upd'})
#
#             print traceback.format_exc()
#             return False
#     finally:
#         # noinspection PyUnboundLocalVariable
#         send_obj.close()
#         del send_obj
#         snd_status['locked'] = False
#         snd_status['send_stamp'] = time.time()
#         eo.update_db(send_id)
#         #del trynum