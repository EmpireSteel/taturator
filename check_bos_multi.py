#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 25.07.13, 17:49
Useful description of the module ''
"""
__author__ = 'Taras.Taturevich'

import socket
import cx_Oracle
from threading import Thread
from Queue import Queue

import myoracle
import ping


pos_info_query = u"""
SELECT t.workstation_id, t.Status_Code, t.employee_id, t.status_timestamp
from GKRETAIL.GK_TENDER_REPOSITORY_STATUS t
WHERE t.Tender_Repository_Id <> 0
and t.workstation_id is not NULL
ORDER BY t.workstation_id
"""

_pos_info_query = u"""
select s.workstation_id,
       t.status_code,
       t.employee_id,
       t.status_timestamp
    from gkretail.gk_workstation_status s
    left join GKRETAIL.GK_TENDER_REPOSITORY_STATUS t on
        s.workstation_id=t.workstation_id
    where s.was_used_flag='J'
"""

bo_addr_query = "select t.city, t.street from gkretail.gk_store_data t"

start_pos_nums = {
    '6': 20,
    '70': 84,
    '134': 148,
    '198': 212
}

pos_statuses = {
    'N': u'Неиспользуемый/Рассчитанный',
    'O': u'<-Вышел из системы',
    'I': u'->Регистрирован',
    'P': u'Перерыв'
}


def get_bosinfo_multi(bonames_list, port, sid, login, pwd):

    def thread_connector(boip, port, sid, login, pwd, container):
        try:
            container.append(myoracle.OracleConnection(boip, port, sid, login, pwd))
        except:
            pass

    def get_bo_info(boip, bo_num):
        #time1 = time.time()
        ret_res = []

        tr = Thread(target=thread_connector, args=(boip, port, sid, login, pwd, ret_res))
        tr.start()
        tr.join(120.0)
        #print u'bo access took {} seconds'.format(time.time() - time1)
        if tr.is_alive():
            tr._Thread__stop()
            raise cx_Oracle.DatabaseError
        try:
            bodb = ret_res[0]  #myoracle.OracleConnection(boip, port, sid, login, pwd)
        except IndexError:
            raise cx_Oracle.DatabaseError
        pos_querydata = bodb.query(pos_info_query)
        bo_addr = bodb.query(bo_addr_query)[0]
        pos_querydata2 = []
        pos_nums_stamps = {}
        for num, (pos_num, pos_status, employee_id, status_timestamp) in enumerate(pos_querydata):
            if pos_num not in pos_nums_stamps or pos_nums_stamps[pos_num][1] < status_timestamp:
                pos_nums_stamps[pos_num] = [num, status_timestamp]

        pos_nums_list = list(pos_nums_stamps.keys())

        for pos_num in pos_nums_list:
            pos_querydata2.append(list(pos_querydata[pos_nums_stamps[pos_num][0]]))

        pos_querydata = pos_querydata2
        last_ip_dot_pos = boip.rfind('.')
        boip_begin = boip[:last_ip_dot_pos + 1]
        last_ip_seg = boip[last_ip_dot_pos + 1:]
        try:
            start_pos_ipnum = start_pos_nums[last_ip_seg]
        except KeyError:
            if 4000 <= bo_num <= 4100:
                start_pos_ipnum = 161
            else:
                raise IOError

        for i, item in enumerate(pos_querydata):
            pos_querydata[i].append(boip_begin + str(item[0] + start_pos_ipnum - 1))

        #bodb.connection.close()
        return pos_querydata, bo_addr

    def run_multi(queue):
        while True:
            num, bo = queue.get()
            #shop_bo = 'BO-{}'.format(bo)
            print '{}. Get IP-addresses for shop {} and its POS'.format(num, bo)

            try:
                boip = socket.gethostbyname(bo)
                bo_infodict[bo], bo_addrs[bo] = get_bo_info(boip, bo)
                bo_ips[bo] = boip
            except socket.gaierror:
                print u'Couldn\'t receive shop {} ip'.format(bo)
                bo_notrespond[bo] = u'Нет доступа к магазину'
            except (cx_Oracle.DatabaseError, cx_Oracle.InterfaceError):
                print u'Couldn\'t access to shop {} database'.format(bo)
                bo_notrespond[bo] = u'Нет доступа к БД магазина'
            except IOError:
                print u'Non-standart POS numeration in shop {}'.format(bo)
                bo_notrespond[bo] = u'Нестандартная нумерация касс в магазине'
            queue.task_done()

    bo_infodict = {}
    bo_addrs = {}
    bo_ips = {}
    bo_notrespond = {}

    queue = Queue()
    for num, bo in enumerate(bonames_list):
        queue.put((num, bo))

    for i in range(10):
        worker = Thread(target=run_multi, args=(queue,))
        worker.setDaemon(True)
        worker.start()
    queue.join()

    return bo_ips, bo_infodict, bo_notrespond, bo_addrs


def ping_BOs(bo_infodict):
    def ping_callback(ip, res):
        ip_pingres[ip] = res
        print ip, u'Доступен' if res else u'Недоступен'

    ip_pingres = {}
    my_ping = ping.Ping(100, 3)
    my_ping.set_callback(ping_callback)
    ips = []
    for item in bo_infodict.values():
        ips.extend([x[4] for x in item])
    my_ping.ping(ips)
    return ip_pingres

