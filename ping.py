#!/usr/bin/env python
# coding: utf-8

from threading import Thread
import subprocess
from Queue import Queue


class Ping(object):
    def __init__(self, threads, req_num=4):
        self.num_threads = threads
        self.req_num = req_num

    def set_callback(self, func):
        self.callback = func

    def ping(self, ips):
        queue = Queue()
        for ip in ips:
            queue.put(ip)

        for i in range(self.num_threads):
            worker = Thread(target=self._pinger, args=(queue,))
            worker.setDaemon(True)
            worker.start()

        queue.join()

    def _pinger(self, queue):
        """Pings subnet"""
        while True:
            ip = queue.get()
            #print "Thread %s: Pinging %s" % (i, ip)
            ret = subprocess.call("ping %s" % ip,
                                  shell=False,
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.PIPE)

            res = True if ret == 0 else False
            self.callback(ip, res)
            queue.task_done()

# def ping_callback(ip, res):
#     print ip, res
#
# num_threads = 4
# #queue = Queue()
# ips = ["10.98.150.212", "10.50.103.134", "10.0.1.11", "10.0.1.51"]
# #wraps system ping command
#
# ping = Ping(num_threads, 4)
# ping.set_callback(ping_callback)
# ping.ping(ips)

# def pinger(i, q):
#     """Pings subnet"""
#     while True:
#         ip = q.get()
#         print "Thread %s: Pinging %s" % (i, ip)
#         ret = subprocess.call("ping %s" % ip,
#                               shell=True,
#                               stdout=subprocess.PIPE,
#                               stderr=subprocess.PIPE)
#         if ret == 0:
#             print "%s: is alive" % ip
#         else:
#             print "%s: did not respond" % ip
#         q.task_done()
# #Spawn thread pool
# for i in range(num_threads):
#
#     worker = Thread(target=pinger, args=(i, queue))
#     worker.setDaemon(True)
#     worker.start()
# #Place work in queue
# for ip in ips:
#     queue.put(ip)
# #Wait until worker threads are done to exit
# queue.join()