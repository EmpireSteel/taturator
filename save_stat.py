#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 08.08.13, 14:22
Useful description of the module ''
"""
import datetime
import time
from os import path
import os
import db
import codecs

__author__ = 'Taras.Taturevich'


def get_pos_grouped_by_hostname(hostname, rows):
    """
    :type hostname: str or unicode
    :type rows: list or tuple
    :rtype: list
    """

    res_rows = []

    if len(rows) > 0:
        res_rows.append(u'<td rowspan={}><a name="{}" />{}</td>{}'.format(len(rows), hostname, hostname, rows[0]))
        res_rows.extend(rows[1:])
    return res_rows


def int2bool(num):
    """
    :type num: int
    """
    return True if num == 1 else False


def wrap_in_cells(cells, params=''):
    """
    :type cells: list or tuple
    :rtype: unicode
    """
    return u'<td {}>{}</td>'.format('{params}',
                                    u'</td>\n<td {params}>'.join(unicode(x) for x in cells)).format(params=params)


def wrap_in_table(rows, header=''):
    """
    :type rows: list or tuple
    :type header: unicode or str or list or tuple
    :rtype: unicode
    """
    if len(rows) == 0:
        return u''

    if type(header) in (list, tuple):
        header_str = ''.join(u'<th>{}</th>'.format(x) for x in header)
    else:
        header_str = header

    return u'<table>{}{}</table>'.format(header_str, '\n'.join(u'<tr>{}</tr>'.format(x) for x in rows))


class FileStat(object):
    def __init__(self, connection, file_id):
        """
        :type connection: sqlite3.Connection
        :type file_id: int
        """
        tb_files = db.Files(connection)

        self.file_id = file_id

        self.file_alias, self.local_path, self.movetopos, self.deflate, self.runscript, stopped = \
            tb_files.get_by_id(file_id, ('alias', 'filepath', 'movetopos', 'deflate', 'runscript', 'stopped'))[0]

        self.filename = path.basename(self.local_path)
        self.filesize = path.getsize(self.local_path)
        self.stopped = True if stopped == 1 else False
        self.stopstage = max(self.movetopos, self.deflate, self.runscript) + 1


class SaveStat(object):

    def __init__(self, connection, stat_dir, template_path, file_id, pos_exclusions):

        """
        :type connection: sqlite3.Connection
        :type stat_dir:str or unicode
        :type template_path:str or unicode
        :type file_id:int
        :type pos_exclusions: tuple or list
        """

        self.conn = connection
        self.tb_bosend = db.BOSend(connection)
        self.tb_possend = db.POSSend(connection)
        self.tb_sendstat = db.SendStat(connection)
        self.pos_exclusions = pos_exclusions
        tb_params = db.Params(connection)
        self.grey_pos_days = int(tb_params['grey_pos_days'])

        self.stat_dir = stat_dir
        if not path.exists(stat_dir):
            os.makedirs(stat_dir)

        self.file_params = FileStat(connection, file_id)

        self.file_id = file_id

        self.stat_file_path = path.join(stat_dir, self.file_params.file_alias + '.html')

        #self.statfilepath = path.join(self.stat_dir, path.splitext(path.split(fparams.filename)[1])[0]) + '.html'

        with open(template_path, 'r') as f:
            self.stat_template = f.read().decode('utf-8', 'replace')

        self.color_finished = 'bgcolor="#00F000"'
        self.color_notfinished = 'bgcolor="#FFFF00"'
        self.color_error = 'bgcolor="#FFA0A0"'
        self.color_unused = 'bgcolor="#C0C0C0"'

    def get_bo_stat(self):
        """
        :rtype : list or tuple
        """
        pass

    def get_pos_stat(self):
        """
        :rtype : list or tuple
        """

        pass

    def get_unpack_stat(self):
        """
        :rtype : list or tuple
        """

        pass

    def get_runscript_stat(self):
        """
        :rtype : list or tuple
        """

        pass

    def save_statfile(self, bonum, bosend, bonotaccess, bonotsent, bo_finished,
                      possend, posnotaccess, posnotsent,
                      bo_finished_num, pos_finished_num, pos_num):
        """
        :type bonum: int
        :type bosend: str or unicode
        :type bonotaccess: str or unicode
        :type bonotsent: str or unicode
        :type bo_finished: str or unicode
        :type possend: str or unicode
        :type posnotaccess: str or unicode
        :type posnotsent: str or unicode
        :rtype : bool
        """
        fparams = self.file_params

        timestamp = unicode(datetime.datetime.now().strftime(u'%d-%m-%Y %H:%M:%S'))

        if bonum > 0:
            bo_percentage = float(bo_finished_num) / bonum * 100
            if pos_num > 0:
                pos_percentage = float(pos_finished_num) / pos_num * 100
            else:
                pos_percentage = 0.00

            res = self.stat_template.format(filename=fparams.filename,
                                            hostnum=bonum,
                                            filesize=fparams.filesize,
                                            timestamp=timestamp,
                                            bostat_tr=bosend,
                                            posstat_tr=possend,
                                            bonotaccess=bonotaccess,
                                            bonotsent=bonotsent,
                                            bosent=bo_finished,
                                            posnotaccess=posnotaccess,
                                            posnotsent=posnotsent,
                                            bo_sent_num=bo_finished_num,
                                            pos_sent_num=pos_finished_num,
                                            bo_percentage=bo_percentage,
                                            pos_percentage=pos_percentage
                                            ).encode('utf-8', 'ignore')

            with open(self.stat_file_path, 'w') as f:
                f.write(res)

    def get_general_stat(self):
        bo_send_query = u""" select {} from bo_send b
                    inner join hosts h on h.id=b.host_id
                    inner join file_params f on f.id = b.file_id
                     where b.file_id=?
                     order by 1"""

        # additional fields : status, speed_avg
        data_fields_keys = ['sendid', 'hostname', 'hostid', 'stage', 'tries', 'speed', 'sent_time', 'locked',
                            'percent', 'restart', 'tries_pos', 'file_stopped', 'curr_error', 'timestamp']
        db_fields_str = u'b.id, h.hostname, h.id, b.stage, b.tries, b.speed, b.sent_time, b.locked, ' \
                        u'b.percent_done_float, b.restart, b.tries_pos, f.stopped, b.curr_error, b.timestamp'

        raw_data = self.tb_bosend.mk_query(bo_send_query.format(db_fields_str), self.file_id)
        raw_data.sort(key=lambda x: x[1])

        data = []

        fparams = self.file_params
        for send_info in raw_data:
            item_dict = dict(zip(data_fields_keys, send_info))

            item_dict['locked'] = int2bool(item_dict['locked'])
            item_dict['restart'] = int2bool(item_dict['restart'])

            if item_dict['percent'] is None:
                item_dict['percent'] = 0.0
            else:
                item_dict['percent'] = float(item_dict['percent'])

            item_dict['file_stopped'] = True if item_dict['file_stopped'] == 1 else 0

            tries = item_dict['tries']
            restart = item_dict['restart']
            locked = item_dict['locked']
            stage = item_dict['stage']
            stopped = item_dict['file_stopped']

            if stopped:
                status = u'Загрузка приостановлена'
            elif tries == 0 and restart:
                status = u'Ожидаю перезапуска'
            elif tries > 0 and restart:
                status = u'Нет связи'
            elif tries == 0 and not locked:
                status = u'Не соединялось'
            elif stage > 0:
                if stage == fparams.movetopos:
                    status = u'Ожидает отправки на кассы'
                elif stage == fparams.deflate:
                    status = u'Ожидает распаковки'
                elif stage == fparams.runscript:
                    status = u'Ожидает выполнения установочного скрипта'
                elif stage == fparams.stopstage:
                    status = u'Обработка завершена'
                else:
                    status = u'Статус не ясен'
            elif locked == 1 and tries > 0:
                status = u'Загружается'
            else:
                status = u'Ожидание'

            item_dict['status'] = status

            bostat = self.tb_sendstat.get_by_bosend(item_dict['sendid'], ('block_sent', 'time_spent'))
            if bostat:
                item_dict['speed_avg'] = sum(x[0] / x[1] for x in bostat) / len(bostat)
            else:
                item_dict['speed_avg'] = 0

            data.append(item_dict)

        return data

    def get_pos_stat(self, send_id):
        """
        :type send_id: int
        :rtype: list, list, list
        """

        # hostname_cell = u'<td rowspan="{}">{}</td>'.format(len(send_pos), hostname)
        #first_row = True
        #first_sent_row = True

        posnotsent_list = []
        posnotaccess_list = []
        posall_list = []

        # color_finished = 'bgcolor="#00F000"'
        # color_notfinished = 'bgcolor="#FFFF00"'
        # color_error = 'bgcolor="#FFA0A0"'

        send_pos = self.tb_possend.get_by_bosend_id(send_id, ('posnum', 'stage', 'posip', 'pos_status',
                                                              'accessible', 'curr_error', 'timestamp'))

        #tries_pos = self.tb_bosend.get_by_id(send_id, 'tries_pos')[0]

        curr_stamp = time.time()

        for posnum, pos_stage, posip, pos_status, ping, curr_error, timestamp in send_pos:

            if posip not in self.pos_exclusions:
                if pos_stage == self.file_params.stopstage - 1:
                    stagename = u'Все выполнено'
                elif pos_stage == 0:
                    stagename = u'Не отправлено'
                elif pos_stage == self.file_params.movetopos:
                    stagename = u'Отправлено'
                elif pos_stage == self.file_params.deflate:
                    stagename = u'Распаковано'
                elif pos_stage == self.file_params.runscript:
                    stagename = u'Выполнен скрипт'
                else:
                    stagename = u'Неизвестно'

                #pos_sent = u'Да' if pos_stage >= self.file_params.movetopos else u'Нет'
                access = u'Да' if ping == 1 else u'Нет'

                stamp_str = datetime.datetime.fromtimestamp(timestamp).strftime("%d.%m.%y  %H:%M:%S")

                pos_row = [posnum, posip, stagename, pos_status, access, curr_error, stamp_str]
                clean_row_str = wrap_in_cells(pos_row, '{bgcolor}')
                if pos_stage == self.file_params.stopstage - 1:
                    posall_list.append(clean_row_str.format(bgcolor=self.color_finished))
                else:
                    if curr_error:
                        row = clean_row_str.format(bgcolor=self.color_error)
                    elif ping == 0 and curr_stamp - timestamp > 60 * 60 * 24 * self.grey_pos_days:
                        row = clean_row_str.format(bgcolor=self.color_unused)
                    else:
                        row = clean_row_str.format(bgcolor=self.color_notfinished)
                    posall_list.append(row)
                    posnotsent_list.append(row)
                    if ping == 0:
                        posnotaccess_list.append(clean_row_str.format(bgcolor=''))

        return posall_list, posnotsent_list, posnotaccess_list

    def process_all(self):

        fparams = self.file_params
        data = self.get_general_stat()

        bosendrows = []
        possendrows = []
        bo_notaccessible_rows = []
        pos_notaccessible_rows = []
        bo_notfinished_rows = []
        pos_notfinished_rows = []
        finished_rows = []

        bosendheader_str = (u'Магазин', u'Этап', u'%', u'Скорость (Кб/c)', u'Время',
                            u'Попыток соединения', u'Ошибка', u'Посл. обновление')

        possendheader_str = (u'Магазин', u'Касса', u'IP-адрес', u'Этап', u'Статус', u'Доступна',
                             u'Ошибка', u'Обращалась к BO')

        # posnotsent_list = []
        # posall_list = []

        for item in data:
            # (send_id, hostname, stagenum, tries, movetopos, speed, sent_time, locked, perc_done,
            #  restart, sendid, pos_tries) in data:
            hostname = item['hostname']
            status = item['status']
            speed = item['speed']
            avg_speed = item['speed_avg']
            sent_time = item['sent_time']
            tries = item['tries']
            perc_done = item['percent']
            #movetopos = item['movetopos']
            send_id = item['sendid']
            stage = item['stage']
            err = item['curr_error']
            timestamp = item['timestamp']

            if status == u'Обработка завершена':
                bg = self.color_finished
            elif err:
                bg = self.color_error
            else:
                bg = self.color_notfinished

            bo_str = wrap_in_cells(('<a href = "#{}">{}</a>'.format(hostname, hostname),
                                    status,
                                    u'{:.2f}'.format(float(perc_done)),
                                    u'{:.1f}({:.1f})'.format(speed / 1024, avg_speed / 1024),
                                    u'{:02d}:{:02d}'.format(int(sent_time) / 60, int(sent_time) % 60),
                                    tries,
                                    err,
                                    datetime.datetime.fromtimestamp(timestamp).strftime("%d.%m.%y  %H:%M:%S"),),
                                   bg)

            bosendrows.append(bo_str)
            if status == u'Нет связи':
                bo_notaccessible_rows.append(bo_str)
            if status == u'Обработка завершена':
                #in (u'Нет связи', u'Загружается', u'Ожидание', u'Не соединялось', u'Ожидаю перезапуска'):
                finished_rows.append(bo_str)
            else:
                bo_notfinished_rows.append(bo_str)

            #Таблица отправки на кассы

            if 0 < fparams.movetopos <= stage:
                posall_list, posnotsent_list, posnotaccess_list = self.get_pos_stat(send_id)
                pos_notfinished_rows.extend(get_pos_grouped_by_hostname(hostname, posnotsent_list))
                pos_notaccessible_rows.extend(get_pos_grouped_by_hostname(hostname, posnotaccess_list))
                possendrows.extend(get_pos_grouped_by_hostname(hostname, posall_list))

        bosendtable = wrap_in_table(bosendrows, bosendheader_str)
        bonotaccessibletable = wrap_in_table(bo_notaccessible_rows, bosendheader_str)
        bonotsenttable = wrap_in_table(bo_notfinished_rows, bosendheader_str)
        finishedtable = wrap_in_table(finished_rows, bosendheader_str)
        possendtable = wrap_in_table(possendrows, possendheader_str)
        posnotaccessibletable = wrap_in_table(pos_notaccessible_rows, possendheader_str)
        posnotsenttable = wrap_in_table(pos_notfinished_rows, possendheader_str)

        bo_finished_num = len(finished_rows)
        pos_num = len(possendrows)
        pos_finished_num = pos_num - len(pos_notfinished_rows)

        self.save_statfile(len(data), bosendtable, bonotaccessibletable, bonotsenttable, finishedtable,
                           possendtable, posnotaccessibletable, posnotsenttable,
                           bo_finished_num, pos_finished_num, pos_num)


def save_log(log_dir, log_file, max_lines, text_lines, encoding='cp1251'):
    """
    :param str or unicode log_dir: logging directory
    :param str or unicode log_file: log file name
    :param int max_lines: maximum log file lines number if exceeded should be cut
    :param list or tuple text_lines: lines added to log
    :return:
    """
    if not path.exists(log_dir):
        os.makedirs(log_dir)
    log_path = path.join(log_dir, log_file)
    if path.exists(log_path):
        # noinspection PyUnusedLocal
        num_lines = sum(1 for line in open(log_path))
    else:
        num_lines = 0

    if num_lines + len(text_lines) > max_lines:
        file_cont = codecs.open(log_path, 'r', encoding).read()
        file_lines = file_cont.split('\n')[num_lines - max_lines + len(text_lines):]
        with codecs.open(log_path, 'w', encoding) as f:
            f.write(u'{}\n'.format(u'\n'.join(file_lines)))

    message_txt = u'{}\n'.format('\n'.join(text_lines))
    with codecs.open(log_path, 'a', encoding) as f:
        f.write(message_txt)