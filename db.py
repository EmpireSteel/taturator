#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 23.07.13, 11:21
Useful description of the module ''
"""
__author__ = 'Taras.Taturevich'

import sqlite3
import time


def wrap_in_tuple(value):
    if type(value) not in (list, tuple):
        return value,
    else:
        return value


def wrap_in_list(value):
    if type(value) == tuple:
        return list(value)
    elif type(value) == list:
        return value
    else:
        return [value]


def _determine_columntype(value, types, colnames, default_column):
    try:
        return colnames[types.index(type(value))]
    except (IndexError, ValueError):
        return default_column


class DbManager(object):

    tablename = None

    def __init__(self, connection):
        """
        @type  connection: sqlite3.Connection
        """

        #self.dbpath = dbpath
        self.conn = connection
        self.lastrowid = None

    def _add_condition_edit_column(self, setcols, setvalues, addcol, addvalue):
        """
        @type setcols:str or unicode or tuple or list
        @type addcol:str or unicode
        """

        if not addcol in setcols:
            setcols = wrap_in_list(setcols)
            setvalues = wrap_in_list(setvalues)
            setcols.append(addcol)
            setvalues.append(addvalue)
        return setcols, setvalues

    def mk_query(self, query, params=()):
        """
        @type query:int unicode
        """

        params = wrap_in_tuple(params)

        curs = self.conn.cursor()
        # noinspection PySimplifyBooleanCheck
        if params == ():
            curs.execute(query)
        else:
            curs.execute(query, params)
        res = curs.fetchall()
        curs.close()
        return res

    def mk_ddl(self, query, params=(), commit=True):
        """
        @type query:str or unicode
        @type commit = bool
        """

        params = wrap_in_list(params)

        curs = self.conn.cursor()
        # noinspection PySimplifyBooleanCheck
        if params == []:
            curs.execute(query)
        else:
            curs.execute(query, params)
        self.lastrowid = curs.lastrowid
        rowcount = curs.rowcount
        curs.close()
        if commit:
            self.conn.commit()
        return rowcount

    def get_all(self, columns='*'):
        """
        @type columns:str or unicode or tuple or list
        """

        columns = ', '.join(wrap_in_tuple(columns))
        return self.mk_query('select {} from {}'.format(columns, self.tablename))

    def get_by_id(self, id_, columns='*'):
        """
        @type id_: int
        @type columns:str or unicode or list or tuple
        """
        return self._get_record_by_column_values('id', id_, columns)

    def _add_record(self, columns, values, commit=True):
        """
        @type columns:str or unicode or list or tuple
        @type commit: bool
        """

        columns = wrap_in_tuple(columns)
        values = wrap_in_tuple(values)

        columns_str = ', '.join(columns)
        # noinspection PyUnusedLocal
        questions_str = ','.join('?' for x in values)
        query = u"""insert into {} ({})
                   values ({})""".format(self.tablename, columns_str, questions_str)
        self.mk_ddl(query, values, commit)
        return self.lastrowid

    def _get_record_by_column_values(self, columns, values, ret_columns='*'):
        """
        @type columns:str or unicode or tuple or list
        @type ret_columns:str or unicode or tuple or list
        """

        columns = wrap_in_tuple(columns)
        values = wrap_in_tuple(values)
        ret_columns = ', '.join(wrap_in_tuple(ret_columns))
        filter_str = ' and '.join('{}=?'.format(x) for x in columns)
        query = """select {} from {} where {}""".format(ret_columns, self.tablename, filter_str)
        return self.mk_query(query, values)

    def _edit_record(self, filtercolumns, filtervalues, setcolumns, setvalues, commit=True):
        """
        @type filtercolumns: str or unicode or tuple or list
        @type setcolumns: str or unicode or tuple or list
        @type commit: bool
        """

        filtercolumns = wrap_in_tuple(filtercolumns)
        filtervalues = wrap_in_tuple(filtervalues)
        setcolumns = wrap_in_tuple(setcolumns)
        setvalues = wrap_in_tuple(setvalues)

        filter_str = ' and '.join('{}=?'.format(x) for x in filtercolumns)
        setstr = ', '.join('{}=?'.format(x) for x in setcolumns)

        query = 'update {} set {} where {}'.format(self.tablename, setstr, filter_str)
        return self.mk_ddl(query, list(setvalues) + list(filtervalues), commit)

    def count(self):
        return self.mk_query('select count(*) from {}'.format(self.tablename))[0][0]

    def _del_byvalue(self, column, value, commit=True):
        """
        @type column:str or unicode
        @type commit: bool
        """

        query = """delete from {} where {}=?""".format(self.tablename, column)
        return self.mk_ddl(query, value, commit)

    def clean_table(self):
        return self.mk_ddl('delete from {}'.format(self.tablename))


class Hosts(DbManager):
    tablename = 'hosts'

    query_bylocalhost = """select {} from hosts h
                            inner join bo_send b on h.id=b.host_id
                            inner join file_params f on b.file_id=f.id
                            where f.filepath=?"""

    query_by_file_id = """select {} from hosts h
                            inner join bo_send b on h.id=b.host_id
                            where b.file_id=?"""

    def __init__(self, connection):
        """
        @type  connection: sqlite3.Connection
        """

        DbManager.__init__(self, connection)

    def _determine_hosttype(self, host):
        """
        @type host: str or unicode
        """
        return _determine_columntype(host, (str, unicode), ('hostname', 'hostname'), 'id')

    def add(self, hostname, ip=None, stage=0, commit=True):
        """
        @type hostname: str or unicode
        @type ip: str or unicode
        @type stage: int
        @type commit: bool
        """
        return self._add_record(('hostname', 'ip', 'stage'),
                                (hostname, ip, stage), commit)
        # query = """insert into hosts (hostname, ip, stage)
        #            values (?,?,?)"""
        # self.mk_ddl(query, (hostname, ip, stage))

    def get_by_stage(self, stage, columns='*'):
        """
        @type stage: int
        @type columns = str or unicode or tuple or list
        """
        return self._get_record_by_column_values('stage', stage, columns)
        # query = """select * from hosts where stage=?"""
        # return self.mk_query(query, stage)

    def get_by_host(self, host, columns='*'):
        """
        @type host: str or unicode
        @type columns: str or unicode or tuple or list
        """

        return self._get_record_by_column_values(self._determine_hosttype(host), host, columns)
        # query = """select * from hosts where id=?"""
        # return self.mk_query(query, id)

    def get_by_localfile(self, filepath, columns='*'):
        """
        @type filepath: str or unicode
        @type columns = str or unicode or tuple or list
        """
        columns = ', '.join(wrap_in_tuple(columns))
        query = self.query_bylocalhost.format(columns)
        return self.mk_query(query, filepath)

    def get_by_send_file_id(self, file_id, columns='*'):
        """
        return all hosts id where exists send object for file with id = file_id

        @type file_id:int
        @type columns = str or unicode or tuple or list
        """
        columns = ', '.join(wrap_in_tuple(columns))
        query = self.query_by_file_id.format(columns)
        return self.mk_query(query, file_id)

    def del_byhost(self, host, commit=True):
        """
        @type host: str or unicode
        @type commit: bool
        """
        return self._del_byvalue(self._determine_hosttype(host), host, commit)

    def set_params(self, host, stage=None, ip=None, timestamp=-1, commit=True):
        """
        @type host: str or unicode
        @type stage: int
        @type ip: str or unicode
        @type timestamp: float
        @type commit: bool
        """
        if timestamp == -1:
            timestamp = time.time()
        filtercol = self._determine_hosttype(host)

        setcols = []
        setvalues = []
        if stage:
            setcols.append('stage')
            setvalues.append(stage)
        if ip:
            setcols.append('ip')
            setvalues.append(ip)

        setcols.append('timestamp')
        setvalues.append(timestamp)

        return self._edit_record(filtercol, host, setcols, setvalues, commit)


class POSes(DbManager):
    tablename = 'pos'

    def __init__(self, connection):
        """
        @type  connection: sqlite3.Connection
        """
        DbManager.__init__(self, connection)

    def add(self, bo_id, pos_num, pos_ip=None, status=0, stage=0):
        return self._add_record(('pos_num', 'bo_id', 'status', 'pos_ip', 'stage'),
                                (pos_num, bo_id, status, pos_ip, stage))

    # def get_by_id(self, pos_id):
    #     return self._get_record_by_column_values('id', pos_id)

    def get_by_bo(self, bo_id):
        return self._get_record_by_column_values('bo_id', bo_id)


class Files(DbManager):
    tablename = 'file_params'

    def __init__(self, connection):
        """
        @type  connection: sqlite3.Connection
        """
        DbManager.__init__(self, connection)

    def add(self, alias, filepath, remotepath, movetopos=0, pospath=u'', deflate=0, runscript=0, deflate_path=u'',
            script_path=u''):
        return self._add_record(('alias', 'filepath', 'remotepath', 'movetopos', 'pospath', 'deflate', 'runscript',
                                 'deflatepath', 'scriptpath', 'timestamp'),
                                (alias, filepath, remotepath, movetopos, pospath, deflate, runscript, deflate_path,
                                 script_path, time.time()))

    def update(self, alias, localpath=None, remotepath=None, movetopos=None, deflate=None, runscript=None,
               pospath=None, deflate_path=None, script_path=None, addrs_style=u'bonum', login=None, password=None):

        curr_res = self.get_by_alias(alias, 'id')

        if len(curr_res) > 0:
            paramslst = []
            paramsnameslst = []
            pairs = ((localpath, 'filepath'),
                     (remotepath, 'remotepath'),
                     (movetopos, 'movetopos'),
                     (deflate, 'deflate'),
                     (runscript, 'runscript'),
                     (pospath, 'pospath'),
                     (deflate_path, 'deflatepath'),
                     (script_path, 'scriptpath'),
                     (addrs_style, 'addrs_style'),
                     (login, 'login'),
                     (password, 'password'))

            for param, param_name in pairs:
                if param is not None:
                    paramslst.append(param)
                    paramsnameslst.append(param_name)

            self.edit(curr_res[0][0], paramsnameslst, paramslst)
            return curr_res[0][0]
        else:
            return self.add(alias, localpath, remotepath, movetopos, )

    def get_by_alias(self, alias, ret_columns='*'):
        return self._get_record_by_column_values('alias', alias, ret_columns)

    def get_notstopped(self, columns='*'):
        return self._get_record_by_column_values('stopped', 0, columns)

    def edit_by_alias(self, alias, editcols, editvalues):
        return self._edit_record('alias', alias, editcols, editvalues)

    def get_by_localpath(self, localpath, ret_columns='*'):
        return self._get_record_by_column_values('filepath', localpath, ret_columns)

    def get_by_host_id(self, host_id, ret_columns='*'):
        return self._get_record_by_column_values('host_id', host_id, ret_columns)

    def remove(self, fl, commit=True):
        if type(fl) in (str, unicode):
            return self._del_byvalue('filepath', commit)
        else:
            return self._del_byvalue('id', commit)

    def edit(self, fl, setcols, setvalues, commit=True):
        if type(fl) in (str, unicode):
            filtercol = 'filepath'
        else:
            filtercol = 'id'

        self._add_condition_edit_column(setcols, setvalues, 'timestamp', time.time())

        return self._edit_record(filtercol, fl, setcols, setvalues, commit)


class SendStat(DbManager):
    tablename = 'send_stat'

    def __init__(self, connection):
        """
        @type  connection: sqlite3.Connection
        """
        DbManager.__init__(self, connection)

    def add(self, send_id, blocksize, time_spent, commit=True):
        self._add_record(('send_id', 'block_sent', 'time_spent', 'timestamp'),
                         (send_id, blocksize, time_spent, time.time()), commit)

    def get_by_bosend(self, bosendid, columns='*'):
        return self._get_record_by_column_values('send_id', bosendid, columns)


class BOSend(DbManager):
    tablename = 'bo_send'

    def __init__(self, connection):
        """
        @type  connection: sqlite3.Connection
        """
        DbManager.__init__(self, connection)

    def add(self, host_id, file_id, stage=0, tries=0, speed=0, sent=0, sent_time=0, restart=0, commit=True):
        self._add_record(("host_id", 'file_id', 'stage', 'tries', 'speed', 'sent', 'sent_time', 'restart', 'timestamp'),
                        (host_id, file_id, stage, tries, speed, sent, sent_time, restart, time.time()), commit)

    def get_by_host_id(self, host_id, columns='*'):
        return self._get_record_by_column_values('host_id', host_id, columns)

    def get_by_file_id(self, file_id, columns='*'):
        return self._get_record_by_column_values('file_id', file_id, columns)

    def edit(self, filtercol, filtervalue, setcols, setvalues, commit=True):
        setcols, setvalues = self._add_condition_edit_column(setcols, setvalues, 'timestamp', time.time())
        return self._edit_record(filtercol, filtervalue, setcols, setvalues, commit)

    def del_by_host(self, host, commit=True):
        column = _determine_columntype(host, (int,), ('host_id',), 'host_name')

        if column == 'host_name':
            query = """DELETE from {} b
                        inner join hosts h on h.id=b.host_id
                        where h.hostname=?""".format(self.tablename)
            return self.mk_ddl(query, host)

        return self._del_byvalue(column, host, commit)


class POSSend(DbManager):
    tablename = 'pos_send'

    def __init__(self, connection):
        """
        @type  connection: sqlite3.Connection
        """
        DbManager.__init__(self, connection)

    def add(self, send_id, posnum, posip='', stage=0, tries=0, commit=True):
        timestamp = time.time()
        self._add_record(("pos_id", 'file_id', 'bosend_id', 'posnum', 'posip', 'stage', 'tries', 'timestamp'),
                        (0, 0, send_id, posnum, posip, stage, tries, timestamp), commit=commit)

    def get_by_bosend_id(self, bosend_id, columns='*', stage=None):
        if stage is None:
            return self._get_record_by_column_values('bosend_id', bosend_id, columns)
        else:
            return self._get_record_by_column_values(('bosend_id', 'stage'), (bosend_id, stage), columns)

    def edit(self, filtercols, filtervalues, setcols, setvalues, commit=True):
        setcols, setvalues = self._add_condition_edit_column(setcols, setvalues, 'timestamp', time.time())
        return self._edit_record(filtercols, filtervalues, setcols, setvalues, commit)

    def del_by_bosend(self, bosend, commit=False):
        self._del_byvalue('bosend_id', bosend, commit=False)


class SendMessages(DbManager):
    tablename = 'send_messages'

    def __init__(self, connection):
        """
        @type  connection: sqlite3.Connection
        """
        DbManager.__init__(self, connection)

    def add(self, send_id, msg_type, msg_text, timestamp):
        if type(msg_text) == str:
            msg_text = unicode(msg_text, errors='ignore')
        self._add_record(('bo_send', 'msg_type', 'msg_text', 'timestamp'),
                         (send_id, msg_type, msg_text, timestamp))


class Params(DbManager):
    tablename = 'parameters'

    def __init__(self, connection):
        """
        @type  connection: sqlite3.Connection
        """
        DbManager.__init__(self, connection)

    def get_value(self, parameter):
        query = """select value from {} where name=?""".format(self.tablename)
        return self.mk_query(query, parameter)[0][0]

    def set_value(self, parameter, value, commit=True):
        query = """update {} set value=? where name=?""".format(self.tablename)
        return self.mk_ddl(query, [value, parameter], commit)

    def add(self, name, value='', commit=True):
        return self._add_record(("name", 'value'),
                                (name, value), commit)

    def check_lock(self, wait=False):
        curr_time = time.time()
        #print curr_time
        lock_state = float(self['system_lock'])
        #print
        refresh_time = float(self['refresh_time'])
        if lock_state >= curr_time - refresh_time * 2:
            if wait:
                time1 = time.clock()
                time2 = time1 + refresh_time * 2
                while time.clock() < time2:
                    time.sleep(1)
                    curr_lock_state = float(self['system_lock'])
                    if curr_lock_state != lock_state and curr_lock_state > 0:
                        return False
        return True

    def update_lock(self):
        curr_time = time.time()
        self['system_lock'] = curr_time

    def release_lock(self):
        self['system_lock'] = '0'

    def __len__(self):
        return self.count()

    def __setitem__(self, key, value):
        rowcount = self.set_value(key, value)
        if rowcount == 0:
            self.add(key, value)

    def __getitem__(self, item):
        try:
            res = self.get_value(item)
        except IndexError:
            raise KeyError
        else:
            return res

    def __delitem__(self, key):
        rowcount = self._del_byvalue('name', key)
        if rowcount == 0:
            raise KeyError

    def __iter__(self):
        pass


def main():
    conn = sqlite3.connect('main.db3')

    hosts = Hosts(conn)

    hosts.add('bo-2222', '111.223.111.111', commit=False)
    hosts.add('bo-2223', '111.223.111.111', commit=False)

    conn.commit()

    print hosts.count()

    print hosts.del_byhost('bo-2222', commit=False)
    print hosts.del_byhost('bo-2223', commit=False)

    conn.commit()

    params = Params(conn)

    try:
        print params['tst']
    except KeyError:
        print 'gotcha, can\'t view tst field which is not exists'

    params['tst'] = 'abc'
    print params['tst']

    try:
        del params['tst2']
    except KeyError:
        print 'gotcha, can\'t delete tst2 field which is not exists'

    del params['tst']

    # hosts = Hosts(conn)
    #
    # hosts.add('bo-2222', '111.223.111.111')
    #
    # print hosts.count()
    #
    # hosts.del_byhost('bo-2222')

    conn.close()

if __name__ == "__main__":
    main()