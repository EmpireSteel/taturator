#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 23.07.13, 15:54

командная строка -

restart alias - перезапустить закачку alias
stop alias    - остановить закачку alias
start alias    - запустить ранее остановленную закачку alias
remove alias - удалить задачу (предварительно должна быть удалена из конфига)

remove_host, restart_host - удалить и перезагрузить хосты, можно указать alias, чтобы удалить/перезагрузить из задачи

Этапы:
-1. Обработать параметры командной строки
0. Прочитать список хостов из файла (1 поток) и записать в БД
1. Получить IP-адреса хостов (100 потоков) и записать в БД
2. Прогрузить файлы на хосты (50 потоков), писать статистику в БД
3. Получить номера касс и их IP для хоста с файлами (50 потоков)
4. Передать файл с хоста на кассы
"""
__author__ = 'Taras.Taturevich'

import sys
from os import path
import sqlite3
from ConfigParser import RawConfigParser, NoOptionError
from functools import partial
import time

import x5
import db
import transfer_multi


port = 22
username = "mgmgkappl"
password = "1"

dbname = 'main.db3'


def check_list_for_uniqueness(lst):
    duplst = []
    for item in lst:
        if item in duplst:
            return False
        duplst.append(item)
    return True


def get_option_with_warn(config, message, section, option):
    try:
        return config.get(section, option)
    except NoOptionError:
        print message.format(section=section, option=option)
    exit(-1)


def parse_command_line(cmdline):
    commands = ['restart', 'stop', 'start', 'stop_host', 'remove', 'resume_host', 'status', 'remove_host', 'restart_host']
    if len(cmdline) == 0:
        return 'process', ''
    cmd = cmdline[0].lower()
    if cmd in commands:
        return cmd, cmdline[1:]
    else:
        print u'Неправильная команда, используйте {}'.format(', '.join(commands))
    exit(-1)


class CommandParams(object):
    #modes = ['add', 'restart_file', 'restart_hosts', 'edit_file', 'clear_hosts', 'clear_files', 'process']

    def __init__(self):
        self.work_mode = None
        self.listfile_name = None
        self.localpath = None
        self.remotepath = None
        self.pospath = None
        self.params = None

        self.mode_handlers = {
            'add': self._on_add,
            'restart_file': self._on_restart_file,
            'restart_hosts': self._on_restart_hosts,
            'edit_file': self._on_edit_file,
            'clear_hosts': self._on_clear_hosts,
            'clear_file': self._on_clear_file,
            'process': self._on_process
        }

    def __get_mandatory_file(self, argnum):
        try:
            fpath = self.params[argnum]
            if path.exists(fpath):
                return path.abspath(fpath)
            print u'Не существует файл {}'.format(fpath)
            exit(-1)
        except IndexError:
            pass

    def _get_arbitrary_file(self, argnum):
        fpath = self._get_arbitrary_param(argnum)
        if fpath != '*' and not path.exists(fpath):
            print u'Не существует файл {}'.format(fpath)
            exit(-1)

        return path.abspath(fpath)

    def _fill_hosts(self, argnum):
        self.listfile_name = self._get_arbitrary_file(argnum)

    def _fill_pospath(self, argnum):
        pospath = self._get_arbitrary_param(argnum, 'False')
        if pospath != 'False':
            self.pospath = pospath
        else:
            self.pospath = None

    def _get_arbitrary_param(self, argnum, default='*'):
        try:
            value = self.params[argnum]
        except IndexError:
            value = default
        return value

    def _get_mandatory_param(self, argnum):
        try:
            value = sys.argv[argnum]
            return value
        except IndexError:
            print u'Не задано значение'
            exit(-1)

    def _on_add(self):
        self.listfile_name = self.__get_mandatory_file(1)
        self.localpath = self.__get_mandatory_file(2)
        self.remotepath = self._get_arbitrary_param(3)
        self._fill_pospath(4)

    def _on_restart_file(self):
        self.localpath = self.__get_mandatory_file(1)

    def _on_restart_hosts(self):
        self.listfile_name = self._get_arbitrary_file(1)
        self.localpath = self._get_arbitrary_file(2)

    def _on_edit_file(self):
        self.localpath = self.__get_mandatory_file(1)
        self.remotepath = self._get_arbitrary_param(2)
        self._fill_pospath(3)

    def _on_clear_hosts(self):
        self._fill_hosts(1)

    def _on_clear_file(self):
        self.localpath = self.__get_mandatory_file(1)

    def _on_process(self):
        return

    def get_fromcmdline(self, commandline):

        self.params = commandline[1:]
        try:
            self.work_mode = commandline[1]
            if self.work_mode not in self.mode_handlers:
                print u'Указан неправильный режим, используйте {}'.format(', '.join(self.mode_handlers.keys()))
                exit(-1)
        except IndexError:
            self.work_mode = 'process'

        self.mode_handlers[self.work_mode]()


class FillDb(object):
    def __init__(self, connection):
        self.connection = connection

        self.tb_hosts = db.Hosts(connection)
        self.tb_files = db.Files(connection)
        self.tb_bosend = db.BOSend(connection)
        self.tb_posend = db.POSSend(connection)
        self.tb_params = db.Params(connection)
        self.tb_send_messages = db.SendMessages(connection)
        self.tb_sendstat = db.SendStat(connection)

    def check_path(self, fpath):
        if path.exists(fpath):
            return path.abspath(fpath)
        print u'Не существует файл {}'.format(fpath)
        exit(-1)

    def check_lock(self, wait=False):

        if not self.tb_params.check_lock(wait):
            return False
        else:
            self.tb_params.update_lock()
            return True

    def release_lock(self):
        self.tb_params.release_lock()

    def fill_config_params(self, configname):
        bo_corr = {
            'login': 'bologin',
            'password': 'bopass',
            'timeout': 'conn_timeout',
            'blocksize': 'blocksize',
            'db_sid': 'ora_sid',
            'db_port': 'ora_port',
            'db_login': 'ora_login',
            'db_password': 'ora_pwd',
            'db_connection_timeout': 'ora_conn_timeout'

        }
        post_corr = {

            'login': 'pos_login',
            'password': 'pos_pwd',
            'min_send_speed': 'pos_send_speed',
            'grey_pos_days': 'grey_pos_days',
            'pos_exclusions': 'pos_exclusions'
        }
        main_corr = {
            'threads': 'bosendthreads',
            'refresh_time': 'refresh_time',
            'start_time': 'start_time',
            'stop_time': 'stop_time',
            'stat_dir': 'stat_dir',
            'stat_template': 'stat_template',
            'log_dir': 'log_dir',
            'max_log_lines': 'max_log_lines'

        }
        config = RawConfigParser()
        config.read(configname)
        db = self.tb_params
        err_message = u'Не задан параметр {} в секции {} конфигурационного файла {}'.format('{option}', '{section}',
                                                                                            configname,)

        get_item = partial(get_option_with_warn, config, err_message)

        for conf_item, db_item in bo_corr.iteritems():
            db[db_item] = get_item('BO', conf_item)

        for conf_item, db_item in main_corr.iteritems():
            db[db_item] = get_item('MAIN', conf_item)

        for conf_item, db_item in post_corr.iteritems():
            db[db_item] = get_item('POST_PROC', conf_item)

    def fill_send_params(self, fpath):
        config = RawConfigParser()
        config.read(fpath)
        aliases = config.sections()
        if not check_list_for_uniqueness(aliases):
            print u'В файле отправок {} повторяющиеся названия'.format(fpath)
            exit(-1)
        tb_files = self.tb_files
        for alias in aliases:
            hostsfile = self.check_path(config.get(alias, 'hosts'))
            #hosts = self._update_hosts_list(hostsfile)

            err_message = u'Не задан параметр {} для отправки {} в файле {}'.format('{option}', '{section}', fpath)
            get_option = partial(get_option_with_warn, config, err_message)

            local_path = self.check_path(get_option(alias, 'local_path'))

            addrs_style = get_option(alias, 'addrs_style')

            remote_path = get_option(alias, 'remote_path').replace('\\', '/')

            if remote_path[-1] in [u'/', u'\\']:
                remote_path = path.join(remote_path, path.basename(local_path))

            try:
                post_actions = [x.strip(' ').lower() for x in config.get(alias, 'post_actions').split(',')]
            except NoOptionError:
                movetopos, deflate, runscript = 0, 0, 0
            else:
                post_action_order = lambda lst, item_name: lst.index(item_name) + 1 if item_name in lst else 0

                movetopos = post_action_order(post_actions, 'movetopos')
                deflate = post_action_order(post_actions, 'deflate')
                runscript = post_action_order(post_actions, 'script')

            if movetopos:
                if addrs_style != 'bonum':
                    print u'{} Вид нумераци отличный от bonum не допускается одновременно ' \
                          u'с отправкой на кассы'.format(alias)
                    exit()

                pos_path = get_option(alias, 'pos_path').replace('\\', '/')
                if pos_path[-1] in [u'/', u'\\']:
                    pos_path = path.join(pos_path, path.basename(local_path))
            else:
                pos_path = u''

            deflate_path = get_option(alias, 'deflate_path') if deflate > 0 else u''
            script_path = get_option(alias, 'script_path') if runscript > 0 else u''
            try:
                login = config.get(alias, 'login')
                password = config.get(alias, 'password')
            except NoOptionError:
                login = None
                password = None

            file_id = tb_files.update(alias, local_path, remote_path, movetopos, deflate, runscript, pos_path,
                                      deflate_path, script_path, addrs_style, login, password)

            self._update_send_tasks(file_id, hostsfile, addrs_style)

    def _update_hosts_list(self, hosts_path, addrs_style):
        """
        Update records about hosts in db in concordance with hosts data in hosts_path
        Returns host names
        """
        if addrs_style == 'bonum':
            hosts_numbers, parse_errors = x5.get_bonames(hosts_path)
            hosts = ['BO-{}'.format(x) for x in hosts_numbers]
        elif addrs_style == 'ip':
            hosts, parse_errors = x5.get_iplist(hosts_path)
        elif addrs_style == 'host':
            hosts, parse_errors = x5.get_hostlist(hosts_path)
        else:
            raise ValueError('Not correct addrs_style, must be "bonum", "ip" or "host"')

        if parse_errors:
            print '\n'.join(parse_errors)

        hosts_all = [x[0] for x in self.tb_hosts.get_all('hostname')]
        new_hosts = [x for x in hosts if x not in hosts_all]

        for host in new_hosts:
            self.tb_hosts.add(host, commit=False)
        self.connection.commit()

        return hosts

    def _update_send_tasks(self, file_id, hostspath, addrs_style):
        hosts = self._update_hosts_list(hostspath, addrs_style)

        actual_filehosts = [x[0] for x in self.tb_hosts.get_by_send_file_id(file_id, 'hostname')]

        for host in hosts:
            if host not in actual_filehosts:
                host_id = self.tb_hosts.get_by_host(host, 'id')[0][0]
                self.tb_bosend.add(host_id, file_id, commit=False)
        self.connection.commit()

    def get_file_id_by_alias(self, alias):
        try:
            return self.tb_files.get_by_alias(alias, 'id')[0][0]
        except IndexError:
            print u'Параметры закачки "{}" еще не добавлены в БД'.format(alias)
            exit(-1)

    def restart_file(self, alias):
        file_id = self.get_file_id_by_alias(alias)
        bosends = [x[0] for x in self.tb_bosend.get_by_file_id(file_id, 'id')]

        for bosend in bosends:
            self.tb_bosend.edit('id', bosend, ('restart', 'stage', 'percent_done_float'), (1, 0, '0'), commit=False)
            self.tb_posend.del_by_bosend(bosend, commit=False)
        self.connection.commit()

    def remove_host(self, host, alias=None):
        """
        :type host: str or unicode
        """

        bosends = self.get_host_bosends(host, alias)
        # host_id = self.tb_hosts.get_by_host('BO-{}'.format(host), 'id')
        # if not alias:
        #     bosends = [x[0] for x in self.tb_bosend.get_by_host_id(host_id, 'id')]
        # else:
        #     file_id = self.get_file_id_by_alias(alias)
        #     bosends = [x[0] for x in self.tb_bosend._get_record_by_column_values(('file_id', 'host_id'),
        #                                                                          (file_id, host_id), 'id')]
        host = self.get_correct_host(host)

        for bosend in bosends:
            self.tb_posend.del_by_bosend(bosend, commit=False)
            self.tb_sendstat._del_byvalue('send_id', bosend, commit=False)
            self.tb_send_messages._del_byvalue('bo_send', bosend, commit=False)
            self.tb_bosend._del_byvalue('id', bosend)
        if alias is None:
            self.tb_hosts.del_byhost(host, commit=False)
        self.connection.commit()

    def get_correct_host(self, host):
        try:
            int(host)
            host = 'BO-{}'.format(host)
        except ValueError:
            pass
        return host

    def get_host_bosends(self, host, alias=None):
        """
        :type host: str or unicode
        :type alias: str or unicode or None
        """
        host = self.get_correct_host(host)

        host_id = self.tb_hosts.get_by_host(host, 'id')[0][0]

        if not alias:
            bosends = [x[0] for x in self.tb_bosend.get_by_host_id(host_id, 'id')]
        else:
            file_id = self.get_file_id_by_alias(alias)
            bosends = [x[0] for x in self.tb_bosend._get_record_by_column_values(('file_id', 'host_id'),
                                                                                 (file_id, host_id), 'id')]

        return bosends

    def restart_host(self, host, alias=None):
        """
        :type host: str or unicode
        :type alias: str or unicode or None
        """
        bosends = self.get_host_bosends(host, alias)
        for bosend in bosends:
            self.tb_bosend.edit('id', bosend, ('restart', 'stage', 'percent_done_float'), (1, 0, '0'), commit=False)
            self.tb_posend.del_by_bosend(bosend, commit=False)
        self.connection.commit()

    def stop_file(self, alias):
        file_id = self.get_file_id_by_alias(alias)
        self.tb_files.edit(file_id, 'stopped', 1)

    def start_file(self, alias):
        file_id = self.get_file_id_by_alias(alias)
        self.tb_files.edit(file_id, 'stopped', 0)

    def remove_file(self, alias):
        file_id = self.get_file_id_by_alias(alias)
        host_names = self.tb_hosts.get_by_send_file_id(file_id, 'hostname')
        for hostname in host_names:
            self.remove_host(hostname[0], alias)
        self.tb_files._del_byvalue('id', file_id)

    def print_status(self):
        tasks = self.tb_files.get_all(('alias', 'stopped'))

        print u'\nСтатус задач:'
        for task in tasks:
            print u'    {} - {}'.format(task[0], u'Запущен' if task[1] == 0 else u'Остановлен')

    def clean_db(self):
        print u'Очищаю БД'
        clean_max_time = time.time() - 24 * 60 * 60 * 60

        clean_messages_query = 'delete from send_messages where timestamp < {}'.format(clean_max_time)
        clean_stat_query = 'delete from send_stat where timestamp < {}'.format(clean_max_time)

        tb_msg = db.SendMessages(self.connection)
        tb_stat = db.SendStat(self.connection)

        msg_deleted = tb_msg.mk_query(clean_messages_query)
        stat_deleted = tb_stat.mk_query(clean_stat_query)

        if msg_deleted + stat_deleted > 0:
            print u'Удалено сообщений {}'.format(len(msg_deleted) + len(stat_deleted))

        self.connection.execute("VACUUM")


def process(connection):
    #Проверяем хосты на доступность и проставляем их IP адреса

    #hosts2check = [x[0] for x in tb_hosts._get_record_by_column_values('status', 0, 'hostname')]

    #bo_ips = check_bos_multi.get_bosinfo_multi(hosts2check, db_port, db_sid, db_login, db_pass)[0]

    #for bo, ip in bo_ips.iteritems():
    #    tb_hosts._edit_record('hostname', bo, ('ip', 'status'), (ip, 1), commit=False)
    #connection.commit()
    transfer_multi.runner(connection)

    print u' Работа программы завершена'


def main():
    db_connection = sqlite3.connect(dbname)
    #::type :FillDb
    setup_db = FillDb(db_connection)
    if not setup_db.check_lock(wait=False):
        print (u'Запущен другой экземпляр программы, второй запуск c той же БД не допускается')
        exit()
    setup_db.fill_config_params('config.conf')
    setup_db.fill_send_params('fileparams.conf')
    setup_db.clean_db()
    work_mode, options = parse_command_line(sys.argv[1:])
#commands = ['restart', 'stop', 'start', 'stop_host', 'start_host', 'status', 'remove_host', 'restart_host']
    if work_mode == 'restart':
        setup_db.restart_file(options[0])
        setup_db.print_status()
    elif work_mode == 'stop_host':
        print u'Not implemented yet'
    elif work_mode == 'resume_host':
        print u'Not implemented yet'
    elif work_mode == 'remove_host':
        setup_db.remove_host(*options)
        setup_db.print_status()
    elif work_mode == 'remove':
        setup_db.remove_file(options[0])
        setup_db.print_status()
    elif work_mode == 'restart_host':
        setup_db.restart_host(*options)
        setup_db.print_status()
    elif work_mode == 'stop':
        setup_db.stop_file(options[0])
        setup_db.print_status()
    elif work_mode == 'status':
        setup_db.print_status()
    elif work_mode == 'start':
        setup_db.start_file(options[0])
        setup_db.print_status()
    elif work_mode == 'process':
        #stage 1
        setup_db.print_status()
        try:
            process(db_connection)
        finally:
            print u'Finishing...'
            setup_db.release_lock()
            db_connection.close()


main()