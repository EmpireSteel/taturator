#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created at: 18.07.13, 11:04
Useful description of the module ''
"""
__author__ = 'Taras.Taturevich'

#import os
from os import path
import time
#import tarfile
import hashlib
import math


def sha256sum_readable(fpath, blocksize=65536):
    hasher = hashlib.sha256()
    with open(fpath, 'rb') as f:
        buf = f.read(blocksize)
        while len(buf) > 0:
            hasher.update(buf)
            buf = f.read(blocksize)
    digest_lst = []
    for symb in hasher.digest():
        hex_num = hex(ord(symb)).replace('0x', '')
        if len(hex_num) == 1:
            hex_num = '0' + hex_num
        digest_lst.append(hex_num)

    return ''.join(digest_lst)


def split_file(fpath, chunksize, chunkmask):
    chunklist = []
    with open(fpath, 'rb') as f:
        chunk = f.read(chunksize)
        n = 0
        while chunk:
            chunkname = chunkmask.format(n)
            with open(chunkname, 'wb') as fw:
                fw.write(chunk)
            n += 1
            chunklist.append(chunkname)
            chunk = f.read(chunksize)
    return chunklist


class FileManager(object):

    def __init__(self, filepath, tmppath, chunkmask=u'{basename}-{size}-{num}.{ext}'):
        self.filepath = filepath
        self.filedir, self.filename = path.split(filepath)
        self.chunkmask = chunkmask
        self.tmppath = tmppath
        #timestamp - штамп времени, начиная с которого данные в кэше считаются актуальными
        self.stampname = '{fname}.stamp'
        self.stamppath = path.join(self.tmppath, self.stampname.format(fname=self.filename))
        self._stamp = None
        self.hashcodes = {}
        self.chunks = {}

        main_hashfile = path.join(tmppath, self.filename + '_hash')

        if not path.exists(main_hashfile):
            old_main_hash = u''
        else:
            with open(main_hashfile, 'r') as f:
                old_main_hash = f.read()
        new_main_hash = self._get_hashcode(filepath)
        if old_main_hash != new_main_hash:
            self.refresh_cache()
            with open(main_hashfile, 'w') as f:
                f.write(new_main_hash)

    def get_main(self):
        return self.filepath

    def get_main_hash(self):
        return self._get_hashcode(self.filepath)

    def get_chunk(self, size, num):

        if num < self.get_chunks_count(size):
            return self._get_chunk_name(size, num)
        else:
            raise IndexError, 'Too big chunk index'

    def get_chunk_hash(self, size, num):
        return self._get_hashcode(self.get_chunk(size, num))

    def get_chunks_count(self, size):
        if size not in self.chunks:
            self._prepare_chunks(size)
        return len(self.chunks[size][0])

    def _get_chunk_mask(self, size, numlen):
        numformat = '{{:0>{}d}}'.format(numlen)
        return path.join(self.tmppath, self.chunkmask.format(basename=self.filename,
                                                             size=size, num=numformat, ext='part'))

    def _get_chunk_name(self, size, num):
        return self.chunks[size][1].format(num)

    def _prepare_chunks(self, size):

        fsize = path.getsize(self.filepath)
        chunksnum = int(math.ceil(fsize / float(size)))
        numlen = len(str(chunksnum))

        chunkmask = self._get_chunk_mask(size, numlen)
        chunk0 = chunkmask.format(0)
        if not path.exists(chunk0):
            chunkstamp = 0
        else:
            chunkstamp = path.getctime(chunk0)

        if chunkstamp < self._get_timestamp():

            names = split_file(self.filepath, size, chunkmask)
        else:
            try:
                for num in xrange(chunksnum):
                    if not path.exists(chunkmask.format(num)):
                        raise StopIteration

                names = [chunkmask.format(x) for x in xrange(chunksnum)]
            except StopIteration:
                names = split_file(self.filepath, size, chunkmask)

        self.chunks[size] = names, chunkmask

    def _get_hashcode(self, fpath):
        if fpath not in self.hashcodes:
            hashcode = sha256sum_readable(fpath)
            self.hashcodes[fpath] = hashcode
        return self.hashcodes[fpath]

    def refresh_cache(self):
        """
        При обновлении файла инциируем пересборку кэша при обращениях
        """
        self._set_timestamp()
        self.hashcodes = {}

    def _set_timestamp(self):
        stamp_value = time.time()
        self._stamp = stamp_value
        with open(self.stamppath, 'w') as f:
            f.write(str(stamp_value))

    def _get_timestamp(self):
        if not self._stamp:
            with open(self.stamppath, 'r') as f:
                self._stamp = float(f.read())
        return self._stamp


def __main():
    #num, tasks = split_file('test.tar', 100000, 'res-{}.part' )
    #print num

    fm = FileManager('test.tar', 'cache')
    chunk_sizes = [1000, 20000, 110000]
    for size in chunk_sizes:
        print fm.get_chunks_count(size)
        print fm.get_chunk(size, 3)
        print fm.get_chunk_hash(size, 6)

    # fname = 'test.tar'
    # fres = 'res-{}.part'
    # hashfile = 'hash.txt'
    # chunk_size = 100000
    # with open(hashfile, 'w') as f:
    #     f.write(sha256sum_readable(fname) + "  " + fname)
    #
    # with open(fname, 'rb') as f:
    #     chunk = f.read(chunk_size)
    #     n = 0
    #     while chunk:
    #         with open(fres.format(n), 'wb') as fw:
    #             fw.write(chunk)
    #         n += 1
    #         chunk = f.read(chunk_size)

__main()