﻿#encoding="utf-8"
#!/usr/bin/env python

#-------------------------------------------------------------------------------
# Name:        oracle connection module
# Purpose:
#
# Author:      Taras.Taturevich
#
# Created:     26.12.2012
# Copyright:   (c) Taras.Taturevich 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import cx_Oracle
from cx_Oracle import DatabaseError
import socket
from threading import Thread


# noinspection PyUnusedLocal
def output_type_handler(cursor, name, defaultType, size,
                        precision, scale):
    if defaultType in (cx_Oracle.STRING, cx_Oracle.FIXED_CHAR):
        return cursor.var(unicode, size, cursor.arraysize)


def connect_db_timeout(ip, port, sid, login, pwd, timeout):
    def thread_connector(ip, port, sid, login, pwd, container):
        # noinspection PyBroadException
        try:
            container.append(OracleConnection(ip, port, sid, login, pwd))
        except:
            pass

    ret_res = []

    tr = Thread(target=thread_connector, args=(ip, port, sid, login, pwd, ret_res))
    tr.start()
    tr.join(timeout)
    #print u'bo access took {} seconds'.format(time.time() - time1)
    if tr.is_alive() or len(ret_res) == 0:
        # noinspection PyUnresolvedReferences
        tr._Thread__stop()
        raise DatabaseError

    else:
        return ret_res[0]


class OracleConnection(object):
    def __init__(self, host, port, sid, login, pwd, timeout=20):
        """
        :param basestring host: db host
        :param int port: db port
        :param basestring sid: db connection sid
        :param basestring pwd: db password
        :param (int or float) timeout: maximum connection time
        """
        self.timeout = timeout
        self._host = host
        self._port = port
        self._sid = sid
        self._login = login
        self._pwd = pwd
        self._dsn = self._get_dsn()
        self.connect()

    def __connect(self):
        self.connection = connect_db_timeout(self._host,
                                             self._port,
                                             self._login,
                                             self._port,
                                             self._pwd,
                                             self.timeout
                                             )

    def connect(self):
        """

        :rtype : None
        """

        socket.setdefaulttimeout(10)
        try:
            self.connection = cx_Oracle.connect(self._login, self._pwd, self._dsn)
            # noinspection PyPropertyAccess
            self.connection.outputtypehandler = output_type_handler
        except socket.timeout:
            raise DatabaseError

    def _get_dsn(self):
        return cx_Oracle.makedsn(self._host, self._port, self._sid)

    def query(self, query):
        """
        :param basestring query: sql query
        :rtype: list
        """
        cursor = self.connection.cursor()
        cursor.execute(query)
        res = cursor.fetchall()
        cursor.close()
        return res

    def change(self, query):
        """
        :param basestring query: dsl sql expression
        :rtype bool
        """
        try:
            cursor = self.connection.cursor()
            cursor.execute(query)
            cursor.close()
        except DatabaseError:
            return False
        return True

    def commit(self):
        """
        :rtype: bool
        """
        return self.change('commit')

    def close(self):
        self.connection.close()


